-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Aug 05, 2015 at 04:23 AM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `dbp`
--

-- --------------------------------------------------------

--
-- Table structure for table `dbp_authors`
--

CREATE TABLE `dbp_authors` (
`author_id` int(11) NOT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_authors`
--

INSERT INTO `dbp_authors` (`author_id`, `author_name`, `created_at`, `updated_at`) VALUES
(1, 'Shaari Isa', NULL, NULL),
(2, 'Ramli Said', NULL, '2015-06-08 19:35:13'),
(3, 'Hasrul Rizwan', '2015-02-18 04:56:34', '2015-02-18 04:56:34'),
(5, 'Fadlin', '2015-02-24 04:37:40', '2015-02-24 04:37:40'),
(15, 'Nik Safiah', '2015-02-24 09:55:34', '2015-02-24 09:55:34'),
(27, 'Jasni Matlani', '2015-02-26 08:20:26', '2015-02-26 08:20:26'),
(28, 'Fatima Saidin', '2015-02-26 09:02:41', '2015-02-26 09:02:41'),
(30, 'Siti Hana Satipan', '2015-05-20 14:20:10', '2015-05-20 14:20:10'),
(31, 'Zainoz', '2015-06-10 02:52:09', '2015-06-10 02:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `dbp_books`
--

CREATE TABLE `dbp_books` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `isbn` varchar(255) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `pages` int(4) DEFAULT NULL,
  `thumb` longtext,
  `pic` longtext,
  `summary` longtext,
  `cat_id` int(11) DEFAULT NULL,
  `pub_id` int(11) DEFAULT NULL,
  `auth_id` int(11) DEFAULT NULL,
  `addby` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `level` int(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `epub` longtext,
  `pdf` longtext
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_books`
--

INSERT INTO `dbp_books` (`id`, `title`, `price`, `isbn`, `year`, `pages`, `thumb`, `pic`, `summary`, `cat_id`, `pub_id`, `auth_id`, `addby`, `created_at`, `updated_at`, `level`, `status`, `epub`, `pdf`) VALUES
(93, 'Pulau Tanpa Cinta', '10.00', NULL, 2014, 333, NULL, 'images/bookcover/93/pulautanpacinta.png', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n', 18, NULL, 1, NULL, '2015-02-26 09:00:39', '2015-06-08 10:19:45', NULL, '1', 'epub-F1.epub', 'pdf-test1 (1).pdf'),
(94, 'Operasi Matrik', '12.00', NULL, 2014, 220, NULL, 'images/bookcover/9789836242563-OperasiMatrik/operasi-matrik.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n', 4, NULL, 5, NULL, '2015-02-26 09:02:51', '2015-06-08 16:57:35', NULL, '1', NULL, NULL),
(95, 'Perutusan Dari Penjara ', '12.80', NULL, 2014, 235, NULL, 'images/books/9789834611430-PerutusanDariPenjara/bookcover//perutusan.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 4, NULL, 1, NULL, '2015-02-26 09:05:19', '2015-06-08 16:41:41', NULL, '1', NULL, NULL),
(99, 'Cintai Alam Kita', '8.00', NULL, 2009, 12, NULL, NULL, 'Melalui buku ini, murid dapat mendengar dan memberi gerak balas terhadap \r\n\r\nperbuatan, menjawab soalan dengan tepat, bersoal jawab secara spontan, bercerita \r\n\r\ntentang hasil kerja atau gambar yang dilihat, membaca dua suku kata yang terdiri \r\n\r\ndaripada (KV+KVK), (KVK+KV) dan (KVK+KVK), menulis perkataan serta menulis \r\n\r\nrangkai kata dan ayat yang bermakna. Di samping kemahiran bahasa, murid juga \r\n\r\ndidedahkan dengan pengetahuan tentang unsur-unsur yang terdapat di bumi seperti \r\n\r\nair, udara dan tanah, dan faedahnya kepada kehidupan manusia.', 18, NULL, 3, NULL, '2015-05-20 14:52:38', '2015-06-08 16:41:00', NULL, '1', NULL, NULL),
(166, 'Formula Wan', '123.00', NULL, 1980, 23, NULL, 'images/bookcover/166/formulawan.png', '“SUASANA di dalam dewan peperiksaan Kolej Nilam sunyi sepi. Semua pelajar tahun akhir pengajian Automotif dan Enjin sedang menduduki peperiksaan. Begitu juga dengan Wan Mulok dan Radi Kholidi, rakan karibnya.\r\nSelepas itu mereka akan menjalani peperiksaan amali di bengkel kereta yang terletak di bahagian belakang bangunan pejabat Kolej Nilam. Sekiranya berjalan lancar, pada petang nanti tamatlah perjuangan para pelajar tahun akhir pengajian Automotif dan Enjin, Institut Kemahiran MARA Ranau itu.\r\n\r\nRamai yang berkira-kira mahu menceburi bidang kerjaya, sama ada bekerja di syarikatsyarikat, bengkel ataupun membuka kedai sendiri. Tidak kurang juga yang mahu menyambung pengajian di universiti selepas itu.”\r\n\r\nExcerpt From: DAENG MA PENNER. “FORMULA WAN.” iBooks. ', NULL, NULL, NULL, NULL, '2015-06-08 16:47:36', '2015-06-10 02:41:03', NULL, '1', 'epub-epub-F1.epub', 'pdf-pdf-test1 (1).pdf'),
(167, 'Buku Engkau', '23.00', NULL, 2012, 12, NULL, 'images/bookcover/167/bukuengkau.png', 'Buku Engkau', NULL, NULL, NULL, NULL, '2015-06-08 16:52:24', '2015-06-10 02:32:17', NULL, '1', 'epub-epub-F1.epub', 'pdf-pdf-test1 (1).pdf'),
(168, 'Formula Wan 2', '10.00', NULL, 2011, 22, NULL, 'images/bookcover//formulawan2.png', '“SUASANA di dalam dewan peperiksaan Kolej Nilam sunyi sepi. Semua pelajar tahun akhir pengajian Automotif dan Enjin sedang menduduki peperiksaan. Begitu juga dengan Wan Mulok dan Radi Kholidi, rakan karibnya.\r\nSelepas itu mereka akan menjalani peperiksaan amali di bengkel kereta yang terletak di bahagian belakang bangunan pejabat Kolej Nilam. Sekiranya berjalan lancar, pada petang nanti tamatlah perjuangan para pelajar tahun akhir pengajian Automotif dan Enjin, Institut Kemahiran MARA Ranau itu.”', NULL, NULL, NULL, NULL, '2015-06-10 02:55:21', '2015-06-10 02:55:22', NULL, '1', 'epub-epub-F1.epub', 'pdf-Building Cross-Platform Apps using Titanium, Alloy, and Appcelerator Cloud Services (2).pdf'),
(169, 'TEST', '10.00', NULL, 1, 12, NULL, NULL, '12', NULL, NULL, NULL, NULL, '2015-06-12 02:28:07', '2015-06-12 02:28:07', NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dbp_books_dbp_authors`
--

CREATE TABLE `dbp_books_dbp_authors` (
  `dbp_books_id` int(11) NOT NULL,
  `dbp_authors_author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_books_dbp_authors`
--

INSERT INTO `dbp_books_dbp_authors` (`dbp_books_id`, `dbp_authors_author_id`) VALUES
(94, 1),
(99, 1),
(166, 1),
(167, 1),
(168, 1),
(93, 2),
(99, 2),
(168, 2),
(169, 2),
(93, 3),
(166, 3),
(94, 5),
(95, 30),
(168, 31);

-- --------------------------------------------------------

--
-- Table structure for table `dbp_books_dbp_category`
--

CREATE TABLE `dbp_books_dbp_category` (
  `dbp_books_id` int(11) NOT NULL,
  `dbp_category_cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_books_dbp_category`
--

INSERT INTO `dbp_books_dbp_category` (`dbp_books_id`, `dbp_category_cat_id`) VALUES
(99, 4),
(167, 4),
(169, 4),
(166, 18),
(166, 19),
(94, 20),
(95, 20),
(168, 20),
(93, 21),
(168, 25);

-- --------------------------------------------------------

--
-- Table structure for table `dbp_books_images`
--

CREATE TABLE `dbp_books_images` (
`images_id` int(11) NOT NULL,
  `images_path` longtext,
  `books_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `temp` longtext
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_books_images`
--

INSERT INTO `dbp_books_images` (`images_id`, `images_path`, `books_id`, `updated_at`, `created_at`, `temp`) VALUES
(4, 'images/books/33-Durararax2Shourr/content/DSC00009.JPG', 72, '2015-02-25 18:26:57', '2015-02-25 18:26:57', NULL),
(5, 'images/books/4444-Durararax2Shou/content/10392437_1540329006212118_9042757507070643773_n.jpg', 73, '2015-02-25 18:28:03', '2015-02-25 18:28:03', NULL),
(6, 'images/books/9789834613723-LogHorizon/content/20140922_162516.jpg', 74, '2015-02-25 18:33:28', '2015-02-25 18:33:22', NULL),
(7, 'images/books/334-LogHorizonv3/content/10412002_780916195279748_6816126550847717663_n.jpg', 75, '2015-02-25 18:40:15', '2015-02-25 18:40:15', NULL),
(8, 'images/books/9789834613723-Durararax2Shourry54/content/2015-02-25_08-58-35.jpg', 76, '2015-02-25 18:41:28', '2015-02-25 18:41:28', NULL),
(9, 'images/books/9789834613723-AAAAAAAA3333333333333/content/2015-02-25_08-58-35.jpg', 77, '2015-02-25 18:45:53', '2015-02-25 18:45:52', NULL),
(10, 'images/books/9789834613723-Durararax2Shou/content/2015-01-26_12-16-37.jpg', 88, '2015-02-25 19:15:51', '2015-02-25 19:15:51', NULL),
(11, 'images/books/9789834613723-Durararax2Shou/content/2015-01-26_12-16-37.jpg', 89, '2015-02-25 19:17:17', '2015-02-25 19:17:17', NULL),
(12, 'images/books/9789834613723-SAO/content/2015-01-26_14-10-38.jpg', 90, '2015-02-25 19:19:05', '2015-02-25 19:19:04', NULL),
(13, 'images/books/9789834613723-SAO/content/2015-01-21_14-24-37.jpg', 92, '2015-02-25 19:20:20', '2015-02-25 19:20:20', NULL),
(14, 'images/books/9789834613723-SAO/content/2015-01-21_15-33-27.jpg', 92, '2015-02-25 19:20:20', '2015-02-25 19:20:20', NULL),
(15, 'images/books/9789834613723-SAO/content/2015-01-25_07-07-55.jpg', 92, '2015-02-25 19:20:20', '2015-02-25 19:20:20', NULL),
(16, 'images/books/9789834613723-SAO/content/2015-01-26_11-39-49.jpg', 92, '2015-02-25 19:20:20', '2015-02-25 19:20:20', NULL),
(17, 'images/books/9789834613723-SAO/content/2015-01-26_12-14-08.jpg', 92, '2015-02-25 19:20:20', '2015-02-25 19:20:20', NULL),
(18, 'images/books/9789834613723-SAO/content/2015-01-26_12-15-02.jpg', 92, '2015-02-25 19:20:20', '2015-02-25 19:20:20', NULL),
(19, 'images/books/9789834613723-SAO/content/2015-01-26_12-16-00.jpg', 92, '2015-02-25 19:20:20', '2015-02-25 19:20:20', NULL),
(20, 'images/books/9789834613723-SAO/content/2015-01-26_12-16-37.jpg', 92, '2015-02-25 19:20:20', '2015-02-25 19:20:20', NULL),
(21, 'images/books/9789834613723-SAO/content/2015-01-26_12-17-10.jpg', 92, '2015-02-25 19:20:20', '2015-02-25 19:20:20', NULL),
(22, 'images/books/9789834613723-SAO/content/2015-01-26_14-10-38.jpg', 92, '2015-02-25 19:20:20', '2015-02-25 19:20:20', NULL),
(28, 'images/books/9789836242563-OperasiMatrik/content/2015-02-16_10-30-59.jpg', 94, '2015-02-26 09:02:51', '2015-02-26 09:02:51', NULL),
(29, 'images/books/9789836242563-OperasiMatrik/content/2015-02-16_13-41-30.jpg', 94, '2015-02-26 09:02:51', '2015-02-26 09:02:51', NULL),
(30, 'images/books/9789836242563-OperasiMatrik/content/2015-02-17_12-52-57.jpg', 94, '2015-02-26 09:02:51', '2015-02-26 09:02:51', NULL),
(31, 'images/books/9789836242563-OperasiMatrik/content/2015-02-23_11-15-06.jpg', 94, '2015-02-26 09:02:52', '2015-02-26 09:02:52', NULL),
(32, 'images/books/9789834611430-PerutusanDariPenjara/content/2015-01-31_20-48-15.jpg', 95, '2015-02-26 09:05:19', '2015-02-26 09:05:19', NULL),
(33, 'images/books/9789834611430-PerutusanDariPenjara/content/2015-02-04_11-59-11.jpg', 95, '2015-02-26 09:05:19', '2015-02-26 09:05:19', NULL),
(34, 'images/books/9789834611430-PerutusanDariPenjara/content/2015-02-16_10-30-59.jpg', 95, '2015-02-26 09:05:19', '2015-02-26 09:05:19', NULL),
(99, 'images/bookcover/temp/preview/pulau-tanpa-cinta.jpg', NULL, '2015-06-08 13:16:04', '2015-06-08 13:16:04', '148'),
(100, 'images/bookcover/temp/preview/bb.png', NULL, '2015-06-08 13:17:20', '2015-06-08 13:17:20', '148'),
(101, 'images/bookcover/temp/preview/aa.png', NULL, '2015-06-08 13:17:43', '2015-06-08 13:17:43', '148'),
(102, 'images/bookcover/temp/preview/aa.png', NULL, '2015-06-08 13:18:09', '2015-06-08 13:18:09', '148'),
(103, 'images/bookcover/temp/preview/bb.png', NULL, '2015-06-08 13:18:40', '2015-06-08 13:18:40', '148'),
(104, 'images/bookcover/temp/preview/aa.png', NULL, '2015-06-08 13:20:11', '2015-06-08 13:20:11', '148'),
(105, 'images/bookcover/temp/preview/pdf.png', NULL, '2015-06-08 13:20:50', '2015-06-08 13:20:50', '148'),
(118, 'images/bookcover/167/preview/sudin.jpeg', 167, '2015-06-09 20:55:00', '2015-06-09 20:55:00', NULL),
(119, 'images/bookcover/167/preview/pulau-tanpa-cinta (1).jpg', 167, '2015-06-09 20:55:04', '2015-06-09 20:55:04', NULL),
(124, 'images/bookcover/166/preview/Screen Shot 2015-06-10 at 10.36.30 AM.png', 166, '2015-06-10 02:38:47', '2015-06-10 02:38:47', NULL),
(125, 'images/bookcover/166/preview/Screen Shot 2015-06-10 at 10.36.43 AM.png', 166, '2015-06-10 02:38:49', '2015-06-10 02:38:49', NULL),
(126, 'images/bookcover/166/preview/Screen Shot 2015-06-10 at 10.37.02 AM.png', 166, '2015-06-10 02:38:52', '2015-06-10 02:38:52', NULL),
(127, 'images/bookcover/166/preview/Screen Shot 2015-06-10 at 10.37.08 AM.png', 166, '2015-06-10 02:38:55', '2015-06-10 02:38:55', NULL),
(128, 'images/bookcover/166/preview/Screen Shot 2015-06-10 at 10.37.32 AM.png', 166, '2015-06-10 02:38:58', '2015-06-10 02:38:58', NULL),
(133, 'images/bookcover/168/preview/Screen Shot 2015-06-10 at 10.37.32 AM.png', 168, '2015-06-12 02:24:58', '2015-06-12 02:24:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dbp_books_level`
--

CREATE TABLE `dbp_books_level` (
`level_id` int(11) NOT NULL,
  `level` varchar(255) DEFAULT NULL,
  `level_name` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_books_level`
--

INSERT INTO `dbp_books_level` (`level_id`, `level`, `level_name`, `updated_at`, `created_at`) VALUES
(1, '1', 'Pra Sekolah', NULL, NULL),
(2, '2', 'Sekolah Rendah', NULL, NULL),
(3, '3', 'Sekolah Menengah', NULL, NULL),
(4, '4', 'Kanak-Kanak', NULL, NULL),
(5, '5', 'Remaja', NULL, NULL),
(6, '6', 'Dewasa', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dbp_category`
--

CREATE TABLE `dbp_category` (
`cat_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `picpath` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_category`
--

INSERT INTO `dbp_category` (`cat_id`, `category_name`, `updated_at`, `created_at`, `picpath`) VALUES
(4, 'Agama & Falsafah', NULL, NULL, NULL),
(17, 'Bahasa', '2015-05-20 14:10:53', '2015-05-20 14:10:34', NULL),
(18, 'Seni & Budaya', '2015-05-20 14:11:09', '2015-05-20 14:11:09', NULL),
(19, 'Novel', '2015-06-03 00:28:48', '2015-05-20 14:11:23', NULL),
(20, 'Cerpen', '2015-05-20 14:12:01', '2015-05-20 14:12:01', NULL),
(21, 'Drama', '2015-05-20 14:12:26', '2015-05-20 14:12:18', NULL),
(22, 'Puisi', '2015-05-20 14:12:46', '2015-05-20 14:12:46', NULL),
(23, 'Sains & Teknologi', '2015-05-20 14:12:59', '2015-05-20 14:12:59', NULL),
(24, 'Umum', '2015-05-20 14:13:17', '2015-05-20 14:13:17', NULL),
(25, 'Kanak-kanak', '2015-05-20 14:13:27', '2015-05-20 14:13:27', NULL),
(26, 'Majalah', '2015-06-03 00:34:20', '2015-06-03 00:34:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dbp_category_genre`
--

CREATE TABLE `dbp_category_genre` (
  `id` int(11) NOT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `genre_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dbp_publishers`
--

CREATE TABLE `dbp_publishers` (
`pub_id` int(11) NOT NULL,
  `publisher_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_publishers`
--

INSERT INTO `dbp_publishers` (`pub_id`, `publisher_name`, `created_at`, `updated_at`) VALUES
(3, 'Dewan Bahasa &amp; Pustaka', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dbp_status`
--

CREATE TABLE `dbp_status` (
`status_id` int(11) NOT NULL,
  `status_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_status`
--

INSERT INTO `dbp_status` (`status_id`, `status_name`) VALUES
(1, 'Diterbitkan'),
(2, 'Tidak Diterbitkan');

-- --------------------------------------------------------

--
-- Table structure for table `dbp_users`
--

CREATE TABLE `dbp_users` (
`id` int(10) NOT NULL,
  `username` varchar(60) DEFAULT NULL,
  `password` varchar(60) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(64) NOT NULL,
  `level` varchar(60) DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `ip_address` longtext,
  `profile_photo` longtext,
  `remember_token` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `token_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_users`
--

INSERT INTO `dbp_users` (`id`, `username`, `password`, `name`, `email`, `level`, `status`, `created_at`, `lastLogin`, `ip_address`, `profile_photo`, `remember_token`, `updated_at`, `token_datetime`) VALUES
(1, 'superadmin', '$2y$10$AWdYmVcIE1xWULRX7GSjYu16CR0UdaYlRSKSfFbJcaSKseQCRc94u', 'Administrator', 'syafiq@gates.my', 'admin', 'A', '2013-11-18 00:00:00', '2014-11-16 17:02:08', '::1', 'images/profile_photo/superadmin/10501667_822055084494995_4412535968466860064_n.jpg', NULL, NULL, NULL),
(17, 'fadlin', '*00A51F3F48415C7D4E8908980D443C29C69B60C9', 'Fadlin', 'fadlin@gates.my', 'member', 'A', '2013-11-18 00:00:00', '2014-11-16 17:02:08', '::1', NULL, NULL, NULL, NULL),
(18, 'admin', '$2y$10$AWdYmVcIE1xWULRX7GSjYu16CR0UdaYlRSKSfFbJcaSKseQCRc94u', 'Mohd Syafiq', 'hazelnuts34@gmail.com', 'admin', 'A', '2015-03-08 18:03:37', NULL, NULL, NULL, 'AkqKJtZkc1ILqLn3TS9tnKk0uCyomiykHeEFCLLdB1raFiaKM9roMU2NgIeS', '2015-06-12 02:18:54', NULL),
(24, NULL, '$2y$10$cieANQ2qDJ.FZku5irTDauc13BEpkuTAHtp8YdzD8L/MjLGpaIe7W', 'zainoz zaini', 'zainoz@gmail.com', 'member', 'A', '2015-06-02 05:11:04', NULL, NULL, NULL, '8Q1NK3l25Q7JFGvxY2VoWc5DO5gTFhkam5E9lInnhjDr87Pm1gnFb5EPLn6U', '2015-06-03 03:16:49', NULL),
(45, 'zainoz.zainqi@gmail.com', '$2y$10$rVxL5axYWhkCY5hD..OoFeOmYkDe/ZH9csgQgVF2uU2r75Z7172uK', 'zainozqj', 'zainoz.zainqi@gmail.com', 'member', 'A', '2015-06-02 06:46:48', NULL, NULL, NULL, 'GIlJw2TWPGzdOO4ZQhBN9hT0lIxAVn2KsphWQOUELjSS5EklVeVX6tmGcmDp', '2015-06-09 20:24:56', NULL),
(53, NULL, '$2y$10$KundID8NBdLdKYHaeLUhweUXFbdV8Y/sABE6P7MXTf/q8wYnsT/KK', 'idbp', 'idbpv1@Gmail.com', 'member', 'IA', '2015-06-03 03:15:02', NULL, NULL, NULL, NULL, '2015-06-03 03:15:02', NULL),
(55, 'zainoz.zaini@gmial.com', '$2y$10$o6YZUyJY.wKxHFfNkXucXuWGRB1u6eodLzYgB7iMB8HvvFS8JukIS', 'Zainoz', 'zainoz.zaini@gmial.com', 'member', 'IA', '2015-06-09 05:05:11', NULL, NULL, NULL, '$2y$10$T1Td/rut3s.9gfXxB0T78u65p8E6cdbCQGo/zuGX3uxbRE5b.vfUW', '2015-06-09 05:05:11', NULL),
(73, 'zainoz.zaini@gmail.com', '$2y$10$5Qbo5..orkMrHQo9e1YAcO0hBfFpMKislvfnifu1QHJc9hnmBNwiO', 'Zainoz Zaini', 'zainoz.zaini@gmail.com', 'member', 'A', '2015-06-10 03:00:28', NULL, NULL, NULL, NULL, '2015-07-14 09:13:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dbp_user_level`
--

CREATE TABLE `dbp_user_level` (
  `code` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_user_level`
--

INSERT INTO `dbp_user_level` (`code`, `description`) VALUES
('admin', 'Pentadbir'),
('member', 'Ahli');

-- --------------------------------------------------------

--
-- Table structure for table `dbp_user_status`
--

CREATE TABLE `dbp_user_status` (
`sid` int(11) NOT NULL,
  `shortcode` varchar(65) DEFAULT NULL,
  `fullcode` varchar(65) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dbp_user_status`
--

INSERT INTO `dbp_user_status` (`sid`, `shortcode`, `fullcode`, `description`) VALUES
(1, 'AS', 'Absolute', '0'),
(3, 'IA', 'inactive', 'Tidak Aktif'),
(4, 'S', 'suspended', 'Digantung'),
(5, 'RS', 'resign', 'Berhenti'),
(6, 'P', 'pending', 'Menunggu'),
(7, 'A', 'active', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dbp_authors`
--
ALTER TABLE `dbp_authors`
 ADD PRIMARY KEY (`author_id`);

--
-- Indexes for table `dbp_books`
--
ALTER TABLE `dbp_books`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dbp_books_dbp_authors`
--
ALTER TABLE `dbp_books_dbp_authors`
 ADD PRIMARY KEY (`dbp_books_id`,`dbp_authors_author_id`), ADD KEY `fk_dbp_books_has_dbp_authors_dbp_authors1_idx` (`dbp_authors_author_id`), ADD KEY `fk_dbp_books_has_dbp_authors_dbp_books_idx` (`dbp_books_id`);

--
-- Indexes for table `dbp_books_dbp_category`
--
ALTER TABLE `dbp_books_dbp_category`
 ADD PRIMARY KEY (`dbp_books_id`,`dbp_category_cat_id`), ADD KEY `fk_dbp_books_has_dbp_category_dbp_category1_idx` (`dbp_category_cat_id`), ADD KEY `fk_dbp_books_has_dbp_category_dbp_books1_idx` (`dbp_books_id`);

--
-- Indexes for table `dbp_books_images`
--
ALTER TABLE `dbp_books_images`
 ADD PRIMARY KEY (`images_id`);

--
-- Indexes for table `dbp_books_level`
--
ALTER TABLE `dbp_books_level`
 ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `dbp_category`
--
ALTER TABLE `dbp_category`
 ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `dbp_category_genre`
--
ALTER TABLE `dbp_category_genre`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dbp_publishers`
--
ALTER TABLE `dbp_publishers`
 ADD PRIMARY KEY (`pub_id`);

--
-- Indexes for table `dbp_status`
--
ALTER TABLE `dbp_status`
 ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `dbp_users`
--
ALTER TABLE `dbp_users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `dbp_user_level`
--
ALTER TABLE `dbp_user_level`
 ADD PRIMARY KEY (`code`);

--
-- Indexes for table `dbp_user_status`
--
ALTER TABLE `dbp_user_status`
 ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dbp_authors`
--
ALTER TABLE `dbp_authors`
MODIFY `author_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `dbp_books`
--
ALTER TABLE `dbp_books`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT for table `dbp_books_images`
--
ALTER TABLE `dbp_books_images`
MODIFY `images_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT for table `dbp_books_level`
--
ALTER TABLE `dbp_books_level`
MODIFY `level_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `dbp_category`
--
ALTER TABLE `dbp_category`
MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `dbp_publishers`
--
ALTER TABLE `dbp_publishers`
MODIFY `pub_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dbp_status`
--
ALTER TABLE `dbp_status`
MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dbp_users`
--
ALTER TABLE `dbp_users`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `dbp_user_status`
--
ALTER TABLE `dbp_user_status`
MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dbp_books_dbp_authors`
--
ALTER TABLE `dbp_books_dbp_authors`
ADD CONSTRAINT `fk_dbp_books_has_dbp_authors_dbp_authors1` FOREIGN KEY (`dbp_authors_author_id`) REFERENCES `dbp_authors` (`author_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_dbp_books_has_dbp_authors_dbp_books` FOREIGN KEY (`dbp_books_id`) REFERENCES `dbp_books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `dbp_books_dbp_category`
--
ALTER TABLE `dbp_books_dbp_category`
ADD CONSTRAINT `fk_dbp_books_has_dbp_category_dbp_books1` FOREIGN KEY (`dbp_books_id`) REFERENCES `dbp_books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_dbp_books_has_dbp_category_dbp_category1` FOREIGN KEY (`dbp_category_cat_id`) REFERENCES `dbp_category` (`cat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
