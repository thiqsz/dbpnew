$('.sortable-list').sortable({
		draggable: '.sortable-item',
		ghostClass: 'sortable-ghost',
		handle: '.sortable-handle'
	});
// tab indicator
	$('.tab-nav').each(function() {
		$(this).append('<div class="tab-nav-indicator"></div>');
		tabSwitch($('.nav > li.active', $(this)), null);
	});

// tab switch
	$(document).on('show.bs.tab', '.tab-nav a[data-toggle="tab"]', function(e) {
	 	tabSwitch($(e.target), $(e.relatedTarget));
	});

	function tabSwitch(newTab, oldTab) {
		var $nav = newTab.closest('.tab-nav'),
		    $navIndicator = $('.tab-nav-indicator', $nav),
		    navOffset = $nav.offset().left,
	 	    navWidth = $nav.width(),
	 	    newTabOffset = newTab.offset().left,
	 	    newTabWidth = newTab.outerWidth();

		if (oldTab != null && oldTab.offset().left > newTabOffset) {
			$navIndicator.addClass('reverse');
			setTimeout(function() {
				$navIndicator.removeClass('reverse');
			}, 450);
		};

	 	$navIndicator.css({
	 		left: (newTabOffset - navOffset),
	 		right: navOffset + navWidth - newTabOffset - newTabWidth
	 	});
	}
// tile
	$(document).on('click', function(e) {
		var $target = $(e.target);

		if ($target.is('[data-toggle="tile"], [data-toggle="tile"] *') && !$target.is('[data-ignore="tile"], [data-ignore="tile"] *')) {
			var $trigger = $target.closest('[data-toggle="tile"]');
			if ($trigger.attr('data-parent') != null) {
				$($trigger.attr('data-parent')).find('.tile-active-show').collapse('hide');
			};
			$(getTargetFromTrigger($trigger)).collapse('toggle');
		} else if ($target.is('[data-dismiss="tile"]')) {
			$target.closest('.tile-collapse').find('.tile-active-show').collapse('hide');
		} else if (!$target.is('.tile-collapse, .tile-collapse *')) {
			tReset();
		};
	});

	function tReset() {
		$('.tile-collapse.active').each(function(index) {
			var $collapse = $('.tile-active-show', $(this));
			if (!$collapse.hasClass('tile-active-show-still')) {
				$collapse.collapse('hide');
			};
		});
	}

// tile hide
	$(document).on('hide.bs.collapse', '.tile-active-show', function() {
		$(this).closest('.tile-collapse').css({
			'-webkit-transition-delay': '',
			'transition-delay': ''
		}).removeClass('active');
	});

// tile show
	$(document).on('show.bs.collapse', '.tile-active-show', function() {
		$(this).closest('.tile-collapse').css({
			'-webkit-transition-delay': '',
			'transition-delay': ''
		}).addClass('active');
	});

// tile wrap animation
	$('.tile-wrap-animation').each(function(index) {
		var tileAnimationDelay = 0,
		    tileAnimationTransform = 100;

		$('> .tile', $(this)).each(function(index) {
			$(this).css({
				'-webkit-transform': 'translate(0, ' + tileAnimationTransform + '%)',
				'-ms-transform': 'translate(0, ' + tileAnimationTransform + '%)',
				'transform': 'translate(0, ' + tileAnimationTransform + '%)',
				'-webkit-transition-delay': tileAnimationDelay + 's',
				'transition-delay': tileAnimationDelay + 's'
			});

			tileAnimationDelay = tileAnimationDelay + 0.1;
			tileAnimationTransform = tileAnimationTransform + 10;
		});
	});

	$(window).on('DOMContentLoaded scroll', function() {
		if ($('.tile-wrap-animation:not(.isinview)').length) {
			tileInView();
		};
	});

	function tileInView() {
		$('.tile-wrap-animation:not(.isinview)').each(function() {
			var $this = $(this);
			if (tileInViewCheck($this)) {
				$this.addClass('isinview');
			};
		});
	}

	function tileInViewCheck(tile) {
		tile = tile[0];

		var rect = tile.getBoundingClientRect();

		return (
			rect.top <= window.innerHeight &&
			rect.right >= 0 &&
			rect.bottom >= 0 &&
			rect.left <= window.innerWidth
		);
	}
// toast
	var toastTimeout;

	$('[data-toggle="toast"]').tooltip({
		animation: false,
		container: '.toast',
		html: true,
		placement: 'bottom',
		template: '<div class="tooltip"><div class="toast-inner tooltip-inner"></div></div>',
		trigger: 'manual'
	});

// toast dismiss
	$(document).on('click', '[data-dismiss="toast"]', function(e) {
		e.preventDefault();
		toastHide(0);
	});

	function toastHide(timer, toast) {
		clearTimeout(toastTimeout);

		toastTimeout = setTimeout(function() {
			$('.toast').removeClass('toast-show');

			if ($('.fbtn-container').length) {
				$('.fbtn-container').css('margin-bottom', '');
			};

			$('.toast-inner').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
				$('.toast-toggled').tooltip('hide').removeClass('toast-toggled');

				if (toast !== null && toast !== undefined) {
					toast.tooltip('show').addClass('toast-toggled');
				} else {
					$('.toast').remove();
				}
			});
		}, timer);
	}

// toast hover
	$(document).on('mouseenter', '.toast', function() {
		clearTimeout(toastTimeout);
	});

	$(document).on('mouseleave', '.toast', function() {
		toastHide(6000);
	});

// toast show
	$(document).on('click', '[data-toggle="toast"]', function() {
		var $this = $(this);

		if (!$('.toast').length) {
			$('body').append('<div class="toast"></div>');
		};

		if (!$this.hasClass('toast-toggled')) {
			if ($('.toast').hasClass('toast-show')) {
				toastHide(0, $this);
			} else {
				$this.tooltip('show').addClass('toast-toggled');
			}
		};
	});

	$(document).on('shown.bs.tooltip', '[data-toggle="toast"]', function() {
		var $this = $(this);
		$('.toast').addClass('toast-show');

//		if ($(window).width() < 768 && $('.fbtn-container').length) {
//			$('.fbtn-container').css('margin-bottom', $('.toast').outerHeight());
//		};

		$('.toast-inner').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
			toastHide(6000);
		});
	});