{
    "id":"222",
    "category":"Kategori 2",
    "tag":["Tag 1","Tag 2","Tag 3"],
    "title": "Title 2",
    "writer":"Penulis 2",
    "ISBN":"ISBN_2",
    "sipnosis": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "price": "RM 202.00",
    "pic": "http://placehold.it/110x160/b&text=loaded%20image",
    "pic_small": "http://placehold.it/110x160/b&text=loaded%20image",
    "samples":[
           "http://placehold.it/90x160/b&text=loaded%20image",
            "http://placehold.it/90x160/b&text=loaded%20image",
            "http://placehold.it/90x160/b&text=loaded%20image",
            "http://placehold.it/90x160/b&text=loaded%20image"
        ]
}