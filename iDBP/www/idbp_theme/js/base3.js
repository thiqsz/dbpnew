

	$('.textarea-autosize').textareaAutoSize();
// get target from trigger
	function getTargetFromTrigger(trigger) {
		var href;
		var target = trigger.attr('data-target')
		    || (href = trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '');

		return target;
	}
// header
	var $header = $('.header'),
	    headerHeight,
	    headerNavMinWidth = 0;

// header affix
	$(window).on('scroll', function() {
		if (window.pageYOffset > headerHeight) {
			$header.addClass('fixed');
		} else {
			$header.removeClass('fixed');
		}
	});

// header height
	function headerHeightCal() {
		headerHeight = $header.height();
	}

// header nav positioning
	if ($('.header-nav-scroll').length) {
		$('.header-nav-scroll .nav > li').each(function(index) {
			var $this = $(this);

			if (index < 3) {
				headerNavMinWidth += $this.width();
			} else {
				return false;
			}
		});
	};

	function headerNavPos() {
		var $headerNav = $('.header-nav-scroll');

		$headerNav.removeClass('pull-down');

		if ($headerNav.width() < headerNavMinWidth) {
			$headerNav.addClass('pull-down');
		} else {
			$headerNav.removeClass('pull-down');
		}
	}
// menu backdrop
	if ((($('html').hasClass('touch') && $('.menu').length) || $('.nav-drawer').length) && !$('.menu-backdrop').length) {
		$('body').append('<div class="menu-backdrop"></div>');
	};

	var menuBD = document.getElementsByClassName('menu-backdrop')[0];

	if (menuBD !== undefined) {
		var menuBDTap = new Hammer(menuBD);

		menuBDTap.on('tap', function(e) {
			if ($('.menu.open').length) {
				mReset();
			};
		});
	};

// menu close
	$(document).on('click', function(e) {
		var $target = $(e.target);

		if ($('.menu.open').length && !$target.is('.fbtn-container *, .menu *')) {
			mReset();
		};
	});
	
	function mReset() {
		var $bd = $('body');

		if ($bd.hasClass('menu-open')) {
			$bd.removeClass('menu-open');
		};

		if ($bd.hasClass('nav-drawer-open')) {
			$bd.removeClass('nav-drawer-open');
		};

		$('.menu-toggle').closest('li.active').removeClass('active');

		if ($('.menu.open .menu-search-focus').length) {
			$('.menu.open .menu-search-focus').blur();
		};

		$('.menu.open').removeClass('open');
	}

// menu open
	$(document).on('click', '.menu-toggle', function(e) {
		e.preventDefault();
		e.stopPropagation();

		var $this = $(this),
		    $thisLi = $this.closest('li'),
		    $thisMenu = $(getTargetFromTrigger($this));

		if ($thisLi.hasClass('active')) {
			mReset();
		} else {
			mReset();

			if ($thisMenu.hasClass('nav-drawer')) {
				$('body').addClass('nav-drawer-open');
			} else {
				$('body').addClass('menu-open');
			}

			$thisLi.addClass('active');
			$thisMenu.addClass('open');

			if ($('.menu.open .menu-search-focus').length) {
				$('.menu.open .menu-search-focus').focus();
			};
		}
	});
// modal iframe
	$(document).on('click', '.modal-close-iframe', function(e) {
		e.preventDefault();

		window.parent.closeModal(getTargetFromTrigger($(this)));
	});

	window.closeModal = function(iframe) {
		$(iframe).modal('hide');
	};