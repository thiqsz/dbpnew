// Filename: RackView
//Likulli amalin syirrah, Walikulli syirratiin fatrah, Famankaanat fatrotuhu ila sunnati, Faqodihtada..
define([
  'jquery',
  'underscore',
  'backbone',
  'blazy',
  'text!module/rack/rack_template.html',
  'text!module/rack/temp/card1.html',
  'text!module/master/others/noInternet_template.html',
  'text!module/master/others/notification_template.html',
  'module/detail/detailView',
  'module/Model/BookCollection'
], function($, _, Backbone,Blazy,template,cardTemplate,noInternetTemplate,notificationTemplate,DetailView,BookList){
    
  
    var RackView = Backbone.View.extend({
        el: null,
        
        initialize:function(el){
            console.log('laod Rack Content..');
            this.$el =el;
            _.bindAll(this, 'detectLoadMore');
             $(window).scroll(this.detectLoadMore); 
            
            this.detailView = new DetailView();
            this.bookList = new BookList();
            
        },
        
        
         events:{
             "click .card":"viewBookDetail",
             "click #btnRefresh":"refresh"
        },
        
       
        
        render: function(category){
            
            this.stateNowIsFetching = false;
            
            this.prevCategory = this.category;
            if(category == undefined){
                 this.category=sessionStorage.getItem('current-category');
            }else{
                this.category = category;                                     
            }
            
            compiledTemplate = _.template(template);
            this.$el.html(compiledTemplate());
            
            this.bookNo=0;
            this.qBook=0;
            this.currentQBook=0;
            this.finishedScroll=true; 
              
            this.cardListRender(0);
            $(window).scrollTop(0);
                     
        },

        
        viewBookDetail:function(e){

            var bookId = $(e.currentTarget).data('bookid');
            var bookCat = $(e.currentTarget).data('bookcat');
            console.log(bookId);
            console.log(bookCat);
            
            this.detailView.render(bookId,bookCat);
        },
        
        loadInternetError:function(){
            var temp = _.template(noInternetTemplate);
            $('#cardList').html(temp());
            this.finishedScroll=false;
        },
        
        renderNotification:function(msg){
               var data={
                    message:msg
               };
               var compiledTemplate = _.template(notificationTemplate);
               $('#notification').html(compiledTemplate(data));
               $('.toast').addClass('toast-show');
               this.resetCategory();
               toastHide(6000,$(this)); 
        },
        
        refresh:function(){
           Backbone.trigger('change-category');
        },
        
        resetCategory:function(){
            if(this.prevCategory !== undefined){
                this.category = this.prevCategory;
            }
            this.qBook = 0;
            this.cardListRender(this.qBook);
        },
        
        
        loadImage:function(){
            
           blazy = new Blazy({
                container: '.bookthumb' ,// Default is window
                success: function(){
                    console.log('loading image');
                },
                error: function(ele, msg){
                    console.log(msg);
                }

            });
            
        },
        
        detectLoadMore:function(){  
                    if(that.qBook <=0){
                        return;
                    }
            
                    console.log("load more.....");                  
                    console.log(this.qBook +"--" +this.currentQBook+'('+this.category+')');
                    that= this;
                    console.log(this.finishedScroll);
                    
                   
                    this.bookList.setValue(this.category,this.qBook,null);
                    
                    var onDataHandler = function(bookList){
                       
                        var data={
                                    bookList:bookList.toJSON()
                                 };
                        
                        var temp = _.template(cardTemplate);
                        
                        console.log(data.bookList.length);
                        if(data.bookList.length>0){
                            $('#loadMoreCardList').replaceWith(temp({loadStatus:"loadMore",defaultBookList:data.bookList}));
                            that.loadImage();
                            that.qBook += data.bookList.length;
                            that.finishedScroll = true;
                            
                        }else{
                            console.log(data.bookList.length);
                            that.finishedScroll = false;
                        }
                        
                        that.currentQBook++;
                        this.stateNowIsFetching = false;
 
                    }
                    
                    var onErrorHandler = function(collection, response, options) {
                        console.log("error load more");
                        this.finishedScroll = false;
                        this.stateNowIsFetching = false;
                    }
                 if(($(window).height()+ $(window).scrollTop())>0.75*$(document).height()&&this.finishedScroll&&!this.stateNowIsFetching){
                        this.bookList.fetch({ error:onErrorHandler, success : onDataHandler});
                        this.finishedScroll = false;
                        this.stateNowIsFetching = true;
                    }
            
            
        },
        
        //!!DELETE
        test:function(){
            alert('test');
            console.log('test'); 
        },
        
        testDummyMaxValue:function(no,max){
          return(no<=max);
        },
        //-delete-!!
        
      
        
        cardListRender:function(qBook){
            that=this;
            this.bookList.setValue(this.category,qBook,null); //url request cat=category&start=bookNo //resource.sampleBookListUrl+this.category+"_"+qBook+'.js'
       
            var onDataHandler = function(bookList){
                console.log(bookList);
                var data={
                            bookList:bookList.toJSON()
                         };
                
                 if(data.bookList.length>0){
                    var temp = _.template(cardTemplate);
                   that.qBook +=data.bookList.length;  $('#cardList').html(temp({loadStatus:"init",defaultBookList:data.bookList}));
                    that.loadImage();
                    
                }else{
                    that.renderNotification(resource.msgEmptyListCategory+" "+that.category);
                }
              
//                that.qBook= qBook;
        
                  
            }
            
            var onErrorHandler = function(collection, response, options) {
                console.log(response);
                if(response.status==404){
                  console.log("error page request"); 
                  that.renderNotification(resource.msg404Error);
                }else{
                  that.loadInternetError();   
                }
                //render warning
            }
            
           
                this.bookList.fetch({ error:onErrorHandler, success : onDataHandler,reset:false});
        }
 
  });
  // Our module now returns our view
  return RackView;
});