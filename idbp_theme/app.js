// Filename: app.js
define([
    'jquery',
    'underscore',
    'backbone',
    'jqueryvalidate',
    'router',
    'config'
], function($, _, Backbone,Validate,Router,Config){
             
    var init = function() {
            console.log("init");
            var router = new Router();
            Backbone.history.start();
        };
    
    return {
        initialize: init
    };
});