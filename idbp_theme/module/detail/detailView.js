// Filename: RackView
//Ya Hayyu Ya Qayyum birahmatika astaghitsu, ashlih lii sya’ni kullahu walaa takilni ila nafsi thorfata ‘ain
define([
  'jquery',
  'underscore',
  'backbone',
  'blazy',
  'text!module/detail/detail_template.html',
  'text!module/detail/temp/readSample_template.html',
  'text!module/detail/temp/otherBook_template.html',
  'text!module/master/others/noInternet_template.html',
  'text!module/master/others/notification_template.html',
  'module/Model/BookDetailsModel',
  'module/Model/BookCollection',
   
], function($, _, Backbone,Blazy,template,readSampleTemplate,otherBookTemplate,noInternetTemplate,notificationTemplate,BookDetails,BookList){
    
    var DetailView = Backbone.View.extend({
        el: '#main',
        
        initialize:function(bookId,category){
            console.log('laod Detail Content..');
            this.bookId = bookId;
            this.category =  category;
        },
         events:{
            "click #btnMore":"gotoCategoryList",
            "click .otherBook":"selectOneOtherBook"
        },
        render: function(){
            console.log('fetch book data..');
            //fetch data
            that= this;
            var onDataHandler = function(collection){
           
                that.renderBookDetails();
            }
            var onErrorHandler = function(collection, response, options) {
                console.log("error - no book to show");
                 if(response.status==404){
                  console.log("error page request"); 
                  that.renderNotification(resource.msg404Error);
                }else{
                  that.loadInternetError();   
                }
            }
            this.book = new BookDetails(this.bookId);
            this.book.fetch({ success : onDataHandler, error:onErrorHandler,dataType: "jsonp"});
//            this.book.fetch({ success : onDataHandler, error:onErrorHandler});
     
        },
        
        //fetch data success handler
        renderBookDetails:function(){
            // Using Underscore we can compile our template with data
            var data={
                        book:this.book.toJSON()
                     };
            
            var compiledTemplate = _.template(template);
            var sampleTemplate = _.template(readSampleTemplate);
            console.log(data);
             this.$el.html(compiledTemplate(data));
            $('#sample-container').html(sampleTemplate({samples:data.book.samples}));
            this.loadImage();
            this.renderOtherBook();
             $(window).scrollTop(0);
        },
         
        selectOneOtherBook:function(e){
//            console.log($(e.currentTarget).data('bookid'));
              this.bookId = $(e.currentTarget).data('bookid');
              this.render();
        },
        
        renderOtherBook:function(){
            that=this;
            this.qBook=0;
            
            bookList = new BookList(this.category,0);
            
            var onDataHandler = function(collection){
                console.log(collection);
                var data={
                        otherBooks:bookList.toJSON()
                     };
                var compiledTemplate = _.template(otherBookTemplate);
               $('#other_book').html(compiledTemplate(data));
               
                if(data.otherBooks.length>3){
                    $('#btnMore').show();
                }else{
                    $('#btnMore').hide();
                }
                
                that.loadImage();
            }
            
            var onErrorHandler = function(collection, response, options) {
                console.log("error");
                $('#btnMore').hide();
            }
            
            bookList.fetch({ error:onErrorHandler, success : onDataHandler});
        },
        loadInternetError:function(){
            var temp = _.template(noInternetTemplate);
            $('.row').html(temp());
            $(window).scrollTop(0);
        },
        renderNotification:function(msg){
               var data={
                    message:msg
               };
               var compiledTemplate = _.template(notificationTemplate);
               $('#notification').html(compiledTemplate(data));
               $('.toast').addClass('toast-show');
               toastHide(6000,$(this));
        },
        
        loadImage:function(){
          blazy = new Blazy({
              success: function(){
                console.log('loading image');
              }
            
            });
            
        },
        
        gotoCategoryList:function(){
            Backbone.trigger('change-category', this.category );//Backbone.trigger('change-category', <CATEGORY> );
        },
        
        test:function(){
            console.log('test DetailView');  
        }

  });
  // Our module now returns our view
  return DetailView;
});