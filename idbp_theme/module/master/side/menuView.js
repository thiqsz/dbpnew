    // Filename: views/project/list
    define([
      'jquery',
      'underscore',
      'backbone',
      'config',
      'text!module/master/side/menu_template.html'
    ], function($, _, Backbone,Config,template){

        var MenuView = Backbone.View.extend({

            el:"#menu",
            
            events:{
                "click #test": "test",
                "click .menu_category": "menuCategory",    
            },

            initialize: function() {
             
            },
            
            test:function(){
              console.log("test MenuView");  
            },
            
            gotoRack:function(cat){
                  //got to rack
              console.log('sending ');
             Backbone.trigger('change-category',cat);//Backbone.trigger('change-category', <CATEGORY> );
                    
            },
            
            
            menuCategory:function(e){
                console.log($(e.currentTarget).data('cat'));
                mReset();
                //default category
                var cat=1;
//                switch(e.currentTarget.id){
//                    case 'cat1':
//                        console.log('1');
//                        cat=1;
//                        break;
//                    case 'cat2':
//                        console.log('2');
//                        cat=2;
//                        break;
//                    case 'cat3':
//                        console.log('3');
//                        cat=3;
//                        break;
//                    case 'cat4':
//                        console.log('4');
//                        cat=4;
//                        break;
//                }
                
                this.gotoRack($(e.currentTarget).data('cat'));
            },


            render: function(){
              console.log("menu view rendered");
              var compiledTemplate = _.template(template);
              this.$el.html(compiledTemplate());
            },


      });

      return MenuView;
    });