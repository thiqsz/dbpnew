/*
Navicat MySQL Data Transfer

Source Server         : Izayoi
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : dbp

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2015-03-24 14:45:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dbp_authors
-- ----------------------------
DROP TABLE IF EXISTS `dbp_authors`;
CREATE TABLE `dbp_authors` (
  `author_id` int(11) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_authors
-- ----------------------------
INSERT INTO `dbp_authors` VALUES ('1', 'Shaari Isa', null, null);
INSERT INTO `dbp_authors` VALUES ('2', 'Fatimah Saidin', null, null);
INSERT INTO `dbp_authors` VALUES ('3', 'Hasrul Rizwan', '2015-02-18 04:56:34', '2015-02-18 04:56:34');
INSERT INTO `dbp_authors` VALUES ('5', 'Fadlin', '2015-02-24 04:37:40', '2015-02-24 04:37:40');
INSERT INTO `dbp_authors` VALUES ('15', 'Nik Safiah', '2015-02-24 09:55:34', '2015-02-24 09:55:34');
INSERT INTO `dbp_authors` VALUES ('27', 'Jasni Matlani', '2015-02-26 08:20:26', '2015-02-26 08:20:26');
INSERT INTO `dbp_authors` VALUES ('28', 'Fatima Saidin', '2015-02-26 09:02:41', '2015-02-26 09:02:41');
INSERT INTO `dbp_authors` VALUES ('29', 'Harima', '2015-02-26 14:55:16', '2015-02-26 14:55:16');

-- ----------------------------
-- Table structure for dbp_books
-- ----------------------------
DROP TABLE IF EXISTS `dbp_books`;
CREATE TABLE `dbp_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `isbn` varchar(255) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `pages` int(4) DEFAULT NULL,
  `thumb` longtext,
  `pic` longtext,
  `summary` longtext,
  `cat_id` int(11) DEFAULT NULL,
  `pub_id` int(11) DEFAULT NULL,
  `auth_id` int(11) DEFAULT NULL,
  `addby` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `level` int(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `epub` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_books
-- ----------------------------
INSERT INTO `dbp_books` VALUES ('93', 'Pulau Tanpa Cinta', '16.00', '9789834613723', '2014', '333', null, 'images/books/9789834613723-PulauTanpaCinta/bookcover//pulau-tanpa-cinta.jpg', '<p>&nbsp;Novel laskar cinta</p>\r\n', '4', '3', '27', null, '2015-02-26 09:00:39', '2015-02-26 09:00:39', '6', '1', null);
INSERT INTO `dbp_books` VALUES ('94', 'Operasi Matrik', '12.00', '9789836242563', '2014', '220', null, 'images/books/9789836242563-OperasiMatrik/bookcover//operasi-matrik.jpg', '<p>Novel</p>\r\n', '4', '3', '28', null, '2015-02-26 09:02:51', '2015-02-26 09:02:51', '6', '1', null);
INSERT INTO `dbp_books` VALUES ('95', 'Perutusan Dari Penjara ', '12.80', '9789834611430', '2014', '235', null, 'images/books/9789834611430-PerutusanDariPenjara/bookcover//perutusan.jpg', '<p>Novel</p>\r\n', '4', '1', '1', null, '2015-02-26 09:05:19', '2015-02-26 09:05:19', '1', '1', null);

-- ----------------------------
-- Table structure for dbp_books_images
-- ----------------------------
DROP TABLE IF EXISTS `dbp_books_images`;
CREATE TABLE `dbp_books_images` (
  `images_id` int(11) NOT NULL AUTO_INCREMENT,
  `images_path` longtext,
  `books_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`images_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_books_images
-- ----------------------------
INSERT INTO `dbp_books_images` VALUES ('4', 'images/books/33-Durararax2Shourr/content/DSC00009.JPG', '72', '2015-02-25 18:26:57', '2015-02-25 18:26:57');
INSERT INTO `dbp_books_images` VALUES ('5', 'images/books/4444-Durararax2Shou/content/10392437_1540329006212118_9042757507070643773_n.jpg', '73', '2015-02-25 18:28:03', '2015-02-25 18:28:03');
INSERT INTO `dbp_books_images` VALUES ('6', 'images/books/9789834613723-LogHorizon/content/20140922_162516.jpg', '74', '2015-02-25 18:33:28', '2015-02-25 18:33:22');
INSERT INTO `dbp_books_images` VALUES ('7', 'images/books/334-LogHorizonv3/content/10412002_780916195279748_6816126550847717663_n.jpg', '75', '2015-02-25 18:40:15', '2015-02-25 18:40:15');
INSERT INTO `dbp_books_images` VALUES ('8', 'images/books/9789834613723-Durararax2Shourry54/content/2015-02-25_08-58-35.jpg', '76', '2015-02-25 18:41:28', '2015-02-25 18:41:28');
INSERT INTO `dbp_books_images` VALUES ('9', 'images/books/9789834613723-AAAAAAAA3333333333333/content/2015-02-25_08-58-35.jpg', '77', '2015-02-25 18:45:53', '2015-02-25 18:45:52');
INSERT INTO `dbp_books_images` VALUES ('10', 'images/books/9789834613723-Durararax2Shou/content/2015-01-26_12-16-37.jpg', '88', '2015-02-25 19:15:51', '2015-02-25 19:15:51');
INSERT INTO `dbp_books_images` VALUES ('11', 'images/books/9789834613723-Durararax2Shou/content/2015-01-26_12-16-37.jpg', '89', '2015-02-25 19:17:17', '2015-02-25 19:17:17');
INSERT INTO `dbp_books_images` VALUES ('12', 'images/books/9789834613723-SAO/content/2015-01-26_14-10-38.jpg', '90', '2015-02-25 19:19:05', '2015-02-25 19:19:04');
INSERT INTO `dbp_books_images` VALUES ('13', 'images/books/9789834613723-SAO/content/2015-01-21_14-24-37.jpg', '92', '2015-02-25 19:20:20', '2015-02-25 19:20:20');
INSERT INTO `dbp_books_images` VALUES ('14', 'images/books/9789834613723-SAO/content/2015-01-21_15-33-27.jpg', '92', '2015-02-25 19:20:20', '2015-02-25 19:20:20');
INSERT INTO `dbp_books_images` VALUES ('15', 'images/books/9789834613723-SAO/content/2015-01-25_07-07-55.jpg', '92', '2015-02-25 19:20:20', '2015-02-25 19:20:20');
INSERT INTO `dbp_books_images` VALUES ('16', 'images/books/9789834613723-SAO/content/2015-01-26_11-39-49.jpg', '92', '2015-02-25 19:20:20', '2015-02-25 19:20:20');
INSERT INTO `dbp_books_images` VALUES ('17', 'images/books/9789834613723-SAO/content/2015-01-26_12-14-08.jpg', '92', '2015-02-25 19:20:20', '2015-02-25 19:20:20');
INSERT INTO `dbp_books_images` VALUES ('18', 'images/books/9789834613723-SAO/content/2015-01-26_12-15-02.jpg', '92', '2015-02-25 19:20:20', '2015-02-25 19:20:20');
INSERT INTO `dbp_books_images` VALUES ('19', 'images/books/9789834613723-SAO/content/2015-01-26_12-16-00.jpg', '92', '2015-02-25 19:20:20', '2015-02-25 19:20:20');
INSERT INTO `dbp_books_images` VALUES ('20', 'images/books/9789834613723-SAO/content/2015-01-26_12-16-37.jpg', '92', '2015-02-25 19:20:20', '2015-02-25 19:20:20');
INSERT INTO `dbp_books_images` VALUES ('21', 'images/books/9789834613723-SAO/content/2015-01-26_12-17-10.jpg', '92', '2015-02-25 19:20:20', '2015-02-25 19:20:20');
INSERT INTO `dbp_books_images` VALUES ('22', 'images/books/9789834613723-SAO/content/2015-01-26_14-10-38.jpg', '92', '2015-02-25 19:20:20', '2015-02-25 19:20:20');
INSERT INTO `dbp_books_images` VALUES ('23', 'images/books/9789834613723-PulauTanpaCinta/content/2015-02-25_08-50-49.jpg', '93', '2015-02-26 09:00:39', '2015-02-26 09:00:39');
INSERT INTO `dbp_books_images` VALUES ('24', 'images/books/9789834613723-PulauTanpaCinta/content/2015-02-25_08-54-54.jpg', '93', '2015-02-26 09:00:39', '2015-02-26 09:00:39');
INSERT INTO `dbp_books_images` VALUES ('25', 'images/books/9789834613723-PulauTanpaCinta/content/2015-02-25_08-57-14.jpg', '93', '2015-02-26 09:00:39', '2015-02-26 09:00:39');
INSERT INTO `dbp_books_images` VALUES ('26', 'images/books/9789834613723-PulauTanpaCinta/content/2015-02-25_08-58-35.jpg', '93', '2015-02-26 09:00:39', '2015-02-26 09:00:39');
INSERT INTO `dbp_books_images` VALUES ('27', 'images/books/9789834613723-PulauTanpaCinta/content/2015-02-26_10-11-55.jpg', '93', '2015-02-26 09:00:39', '2015-02-26 09:00:39');
INSERT INTO `dbp_books_images` VALUES ('28', 'images/books/9789836242563-OperasiMatrik/content/2015-02-16_10-30-59.jpg', '94', '2015-02-26 09:02:51', '2015-02-26 09:02:51');
INSERT INTO `dbp_books_images` VALUES ('29', 'images/books/9789836242563-OperasiMatrik/content/2015-02-16_13-41-30.jpg', '94', '2015-02-26 09:02:51', '2015-02-26 09:02:51');
INSERT INTO `dbp_books_images` VALUES ('30', 'images/books/9789836242563-OperasiMatrik/content/2015-02-17_12-52-57.jpg', '94', '2015-02-26 09:02:51', '2015-02-26 09:02:51');
INSERT INTO `dbp_books_images` VALUES ('31', 'images/books/9789836242563-OperasiMatrik/content/2015-02-23_11-15-06.jpg', '94', '2015-02-26 09:02:52', '2015-02-26 09:02:52');
INSERT INTO `dbp_books_images` VALUES ('32', 'images/books/9789834611430-PerutusanDariPenjara/content/2015-01-31_20-48-15.jpg', '95', '2015-02-26 09:05:19', '2015-02-26 09:05:19');
INSERT INTO `dbp_books_images` VALUES ('33', 'images/books/9789834611430-PerutusanDariPenjara/content/2015-02-04_11-59-11.jpg', '95', '2015-02-26 09:05:19', '2015-02-26 09:05:19');
INSERT INTO `dbp_books_images` VALUES ('34', 'images/books/9789834611430-PerutusanDariPenjara/content/2015-02-16_10-30-59.jpg', '95', '2015-02-26 09:05:19', '2015-02-26 09:05:19');

-- ----------------------------
-- Table structure for dbp_books_level
-- ----------------------------
DROP TABLE IF EXISTS `dbp_books_level`;
CREATE TABLE `dbp_books_level` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(255) DEFAULT NULL,
  `level_name` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_books_level
-- ----------------------------
INSERT INTO `dbp_books_level` VALUES ('1', '1', 'Pra Sekolah', null, null);
INSERT INTO `dbp_books_level` VALUES ('2', '2', 'Sekolah Rendah', null, null);
INSERT INTO `dbp_books_level` VALUES ('3', '3', 'Sekolah Menengah', null, null);
INSERT INTO `dbp_books_level` VALUES ('4', '4', 'Kanak-Kanak', null, null);
INSERT INTO `dbp_books_level` VALUES ('5', '5', 'Remaja', null, null);
INSERT INTO `dbp_books_level` VALUES ('6', '6', 'Dewasa', null, null);

-- ----------------------------
-- Table structure for dbp_category
-- ----------------------------
DROP TABLE IF EXISTS `dbp_category`;
CREATE TABLE `dbp_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `picpath` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_category
-- ----------------------------
INSERT INTO `dbp_category` VALUES ('4', 'Kesusasteraan (Novel &amp; Cerpen)', null, null, null);
INSERT INTO `dbp_category` VALUES ('16', 'Novel3', '2015-02-26 14:58:15', '2015-02-26 14:58:15', null);

-- ----------------------------
-- Table structure for dbp_category_genre
-- ----------------------------
DROP TABLE IF EXISTS `dbp_category_genre`;
CREATE TABLE `dbp_category_genre` (
  `id` int(11) NOT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `genre_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_category_genre
-- ----------------------------

-- ----------------------------
-- Table structure for dbp_publishers
-- ----------------------------
DROP TABLE IF EXISTS `dbp_publishers`;
CREATE TABLE `dbp_publishers` (
  `pub_id` int(11) NOT NULL AUTO_INCREMENT,
  `publisher_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_publishers
-- ----------------------------
INSERT INTO `dbp_publishers` VALUES ('1', 'Alaf21', null, null);
INSERT INTO `dbp_publishers` VALUES ('2', 'Telaga Biru', null, null);
INSERT INTO `dbp_publishers` VALUES ('3', 'Dewan Bahasa &amp; Pustaka', null, null);

-- ----------------------------
-- Table structure for dbp_status
-- ----------------------------
DROP TABLE IF EXISTS `dbp_status`;
CREATE TABLE `dbp_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_status
-- ----------------------------
INSERT INTO `dbp_status` VALUES ('1', 'Diterbitkan');
INSERT INTO `dbp_status` VALUES ('2', 'Tidak Diterbitkan');

-- ----------------------------
-- Table structure for dbp_users
-- ----------------------------
DROP TABLE IF EXISTS `dbp_users`;
CREATE TABLE `dbp_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(64) NOT NULL,
  `level` varchar(60) NOT NULL,
  `status` varchar(60) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `ip_address` longtext,
  `profile_photo` longtext,
  `remember_token` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_users
-- ----------------------------
INSERT INTO `dbp_users` VALUES ('1', 'superadmin', '*00A51F3F48415C7D4E8908980D443C29C69B60C9', 'Administrator', 'syafiq@gates.my', 'super', 'A', '2013-11-18 00:00:00', '2014-11-16 17:02:08', '::1', 'images/profile_photo/superadmin/10501667_822055084494995_4412535968466860064_n.jpg', null, null);
INSERT INTO `dbp_users` VALUES ('17', 'fadlin', '*00A51F3F48415C7D4E8908980D443C29C69B60C9', 'Fadlin', 'fadlin@gates.my', 'member', 'A', '2013-11-18 00:00:00', '2014-11-16 17:02:08', '::1', null, null, null);
INSERT INTO `dbp_users` VALUES ('18', 'admin', '$2y$10$AWdYmVcIE1xWULRX7GSjYu16CR0UdaYlRSKSfFbJcaSKseQCRc94u', 'Mohd Syafiq', 'hazelnuts34@gmail.com', '', null, '2015-03-08 18:03:37', null, null, null, 'FQjw2tifeUhQ136qF3DD9OYZDtCZy8pnu7FiyKNn7PYgNQaRXhBdFKr0Pw6w', '2015-03-08 18:03:37');

-- ----------------------------
-- Table structure for dbp_user_level
-- ----------------------------
DROP TABLE IF EXISTS `dbp_user_level`;
CREATE TABLE `dbp_user_level` (
  `code` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_user_level
-- ----------------------------
INSERT INTO `dbp_user_level` VALUES ('admin', 'Administrator');
INSERT INTO `dbp_user_level` VALUES ('member', 'Member');
INSERT INTO `dbp_user_level` VALUES ('staff', 'Staff');
INSERT INTO `dbp_user_level` VALUES ('super', 'Super Administrator');

-- ----------------------------
-- Table structure for dbp_user_status
-- ----------------------------
DROP TABLE IF EXISTS `dbp_user_status`;
CREATE TABLE `dbp_user_status` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `shortcode` varchar(65) DEFAULT NULL,
  `fullcode` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dbp_user_status
-- ----------------------------
INSERT INTO `dbp_user_status` VALUES ('1', 'AS', 'Absolute');
INSERT INTO `dbp_user_status` VALUES ('2', 'A', 'Active');
INSERT INTO `dbp_user_status` VALUES ('3', 'IA', 'Inactive');
INSERT INTO `dbp_user_status` VALUES ('4', 'S', 'Suspended');
INSERT INTO `dbp_user_status` VALUES ('5', 'RS', 'Resigned');
INSERT INTO `dbp_user_status` VALUES ('6', 'P', 'Pending');
