<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model {
	protected $table = 'dbp_authors';
	protected $primaryKey = 'author_id';

}
