<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DBPUsers extends Model {

    protected $table = 'dbp_users';
	protected $primaryKey = 'uid';

}
