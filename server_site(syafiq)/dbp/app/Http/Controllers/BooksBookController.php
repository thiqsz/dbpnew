<?php namespace App\Http\Controllers;

use Session;
use Validator;
use Input;
use Redirect;
use App\Books;
use App\Author;
use App\Category;
use App\BookLevel;
use App\Publishers;
use App\DBPStatus;
use App\BooksImages;
use View;
use Response;
use File;
use Request;
use Auth;
use DB;
use GuzzleHttp\Url;

class BooksBookController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $name = Auth::user()->name;
        $books = Books::leftJoin('dbp_category', 'dbp_books.cat_id', '=', 'dbp_category.cat_id')->leftJoin('dbp_authors', 'dbp_books.auth_id', '=', 'dbp_authors.author_id')->leftJoin('dbp_publishers', 'dbp_books.pub_id', '=', 'dbp_publishers.pub_id')->leftJoin('dbp_books_level', 'dbp_books.level', '=', 'dbp_books_level.level_id')->get();
        return View::make('index')
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'books.book', array('books' => $books))
            ->nest('footer_script', 'footer_script_datatables')
            ->with('name', $name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $authors = Author::lists('author_name', 'author_id');
        $category = Category::lists('category_name', 'cat_id');
        $booklevel = BookLevel::lists('level_name', 'level_id');
        $publishers = Publishers::lists('publisher_name', 'pub_id');
        $status = DBPStatus::lists('status_name', 'status_id');
        return View::make('index')
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'books.book.create', array('authors' => $authors, 'category' => $category, 'booklevel' => $booklevel, 'publishers' => $publishers, 'status' => $status))
            ->nest('footer_script', 'footer_script_forms');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = array(
            'title' => 'required',
            'price' => 'required',
            'author' => 'required',
            'category' => 'required',
            'isbn' => 'required',
            'year' => 'required',
            'pages' => 'required',
            'booklevel' => 'required',
            'bookcover' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
            'bookcontentepub' => 'required|mimes:epub',
            'bookcontent' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
            'summary' => 'required'
        );


        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('books/book/create')
                ->withErrors($validator);
        } else {
            $book = new Books;
            if (Input::file('bookcover')->isValid()) {
                $book->title = Input::get('title');
                $book->price = Input::get('price');
                $book->auth_id = Input::get('author');
                $book->cat_id = Input::get('category');
                $book->pub_id = Input::get('publisher');
                $book->isbn = Input::get('isbn');
                $book->level = Input::get('booklevel');
                $book->year = Input::get('year');
                $book->pages = Input::get('pages');
                $book->summary = Input::get('summary');
                $book->status = Input::get('status');
                $bookname = Input::file('bookcover')->getClientOriginalName();
                $bookcontent = Input::file('bookcontentepub')->getClientOriginalName();
                $titlename = preg_replace('/[^A-Za-z0-9\-]/', '', $book->title);
                $destinationPath = public_path() . '/images/books/' . $book->isbn . '-' . $titlename . '/bookcover/';
                $destinationPath4Content = public_path() . '/images/books/' . $book->isbn . '-' . $titlename . '/epub/';
                $destinationPath2 = 'images/books/' . $book->isbn . '-' . $titlename . '/bookcover/';
                $bookDestination = $destinationPath2 . '/' . $bookname;
                $bookContentDest = $destinationPath4Content . '/' . $bookcontent;
                if (!file::exists($destinationPath)) {
                    file::makeDirectory($destinationPath, 0777, true, true);
                    Input::file('bookcover')->move($destinationPath, $bookname);
                }
                if (!file::exists($destinationPath4Content)) {
                    file::makeDirectory($destinationPath4Content, 0777, true, true);
                    Input::file('bookcontentepub')->move($destinationPath4Content, $bookcontent);
                }
                $book->pic = $bookDestination;
                $book->epub = $bookContentDest;
                $book->save();
                $latest_id = $book->id;

                $assetPath = public_path() . '/images/books/' . $book->isbn . '-' . $titlename . '/content';
                $assetPath2 = 'images/books/' . $book->isbn . '-' . $titlename . '/content';
                $results = array();

                file::makeDirectory($assetPath, 0777, true, true);

                $files = Input::file('files');
                foreach ($files as $file) {

                    $file->move($assetPath, $file->getClientOriginalName());
                    $image_path = $assetPath2 . '/' . $file->getClientOriginalName();
                    $results[] = $image_path;
                }


                foreach ($results as $path4image) {
                    $booksimage = new BooksImages;
                    $booksimage->images_path = $path4image;
                    $booksimage->books_id = $latest_id;
                    $booksimage->save();
                }


                Session::flash('message', 'Successfully add new book!');
                return Redirect::to('books/book');
            } else {
                Session::flash('message', 'The uploaded images is invalid!');
                return Redirect::to('books/book/create');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        // get the book
        $books = Books::find($id);
        if (is_null($books)) {
            return Redirect::to('books/book');
        }
        $books = Books::leftJoin('dbp_authors', 'dbp_books.auth_id', '=', 'dbp_authors.author_id')
            ->leftJoin('dbp_category', 'dbp_books.cat_id', '=', 'dbp_category.cat_id')
            ->leftJoin('dbp_publishers', 'dbp_books.pub_id', '=', 'dbp_publishers.pub_id')
            ->leftJoin('dbp_books_level', 'dbp_books.level', '=', 'dbp_books_level.level_id')
            ->where('dbp_books.id', '=', $id)
            ->first();
        // {
        // 	$join->on('dbp_books.writer','=','dbp_authors.author_id')
        // 	->where('dbp_books.id','=',24);
        // })


        // show the view and pass the books to it
        return View::make('index')
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'books.book.show', array('books' => $books))
            ->nest('footer_script', 'footer_script_forms');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $books = Books::find($id);
        $bookimages = DB::table('dbp_books_images')->where('books_id', '=', $id)->get();
        if (is_null($books)) {
            return Redirect::to('books/book');
        }
        if (isset($books->status)) {
        }
        $authors = Author::lists('author_name', 'author_id');
        $category = Category::lists('category_name', 'cat_id');
        $booklevel = BookLevel::lists('level_name', 'level_id');
        $publishers = Publishers::lists('publisher_name', 'pub_id');
        $status = DBPStatus::lists('status_name', 'status_id');
        return View::make('index')
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'books.book.edit', array('authors' => $authors, 'category' => $category, 'booklevel' => $booklevel, 'publishers' => $publishers, 'books' => $books, 'status' => $status, 'bookimages' => $bookimages))
            ->nest('footer_script', 'footer_script_forms');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $rules = array(
            'title' => 'required',
            'price' => 'required',
            'author' => 'required',
            'category' => 'required',
            'isbn' => 'required',
            'year' => 'required',
            'pages' => 'required',
            'booklevel' => 'required',
            'bookcover' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
            'summary' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('books/book/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            // store
            $book = Books::find($id);

            $book->title = Input::get('title');
            $book->price = Input::get('price');
            $book->auth_id = Input::get('author');
            $book->cat_id = Input::get('category');
            $book->pub_id = Input::get('publisher');
            $book->isbn = Input::get('isbn');
            $book->level = Input::get('booklevel');
            $book->year = Input::get('year');
            $book->pages = Input::get('pages');
            $book->summary = Input::get('summary');
            $bookname = Input::file('bookcover')->getClientOriginalName();
            $titlename = preg_replace('/[^A-Za-z0-9\-]/', '', $book->title);
            $destinationPath = 'images/bookcover/' . $book->isbn . '-' . $titlename . '';
            $bookDestination = $destinationPath . '/' . $bookname;
            if (!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true, true);
                Request::File('bookcover')->move($destinationPath, $bookname);
            }
            $book->pic = $bookDestination;
            $book->save();

            // redirect
            Session::flash('message', 'Successfully updated book!');
            return Redirect::to('books/book');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //delete books
        Books::find($id)->delete();
        return Redirect::to('books/book')
            ->withInput()
            ->with('message', 'Successfully deleted Book.');
    }

    public function api()
    {


        $bookcat = Category::get();
        $bookarray = array('Category' => array($this->categorylist($bookcat) => $this->booklist($bookcat)));
        //return View::make('api')->json($bookarray);
      return Response::make(json_encode($bookarray, JSON_PRETTY_PRINT))->header('Content-Type', "application/json");

    }

    public function categorylist($catlist)
    {
        foreach ($catlist as $list) {

            $bookcatlist = $list->category_name;
            return $bookcatlist;
        }

    }

    public function booklist($catlist)
    {

        foreach ($catlist as $list) {
            $bookarray = Books::where('cat_id', $list->cat_id)->get();


           // var_dump($books);
            foreach($bookarray as $booksa) {
                $books = $this->bookcontentsample($booksa->id);
                $booksa->sample_content = $books;

                $tempArray = json_decode($bookarray, true);
                //$tempArray2 = json_decode($booksa, true);

            }
            $bookarrays = $tempArray;
            return $bookarrays;
        }


    }

    public function bookcontentsample($booksid)
    {


            $bookimage = BooksImages::where('books_id', $booksid)->get();
            return $bookimage;


    }


}
