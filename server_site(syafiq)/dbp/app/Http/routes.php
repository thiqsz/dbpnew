<?php
use App\Books;
use App\Author;
use App\Category;
use App\BookLevel;
use App\Publishers;
use App\DBPStatus;
use App\BooksImages;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/login', 'HomeController@showLogin');
Route::post('/login','HomeController@doLogin');
Route::get('/register', 'HomeController@showRegister');
Route::post('/register', 'HomeController@toRegister');
Route::filter('auth', function()
{
    if (Auth::guest()) return Redirect::to('login');
});


Route::filter('auth.basic', function()
{
    return Auth::basic();
});

Route::filter('guest', function()
{
    if (Auth::check()) return Redirect::to('dashboard');
});
// Route::group(array('before' => 'auth'), function(){
    Route::resource('/', 'DashboardController');

    Route::resource('/dashboard',  'DashboardController');

    Route::resource('/books/book',  'BooksBookController');

    Route::resource('/users/all',  'UsersAllController');

    Route::resource('/books/category',  'BooksCategoryController');

    Route::resource('/books/author',  'BooksAuthorController');

    Route::resource('/books/publisher',  'BooksPublisherController');

    Route::resource('/books/level', 'BooksLevelController');

    Route::get('/logout',  'HomeController@doLogout');

    Route::resource('/books/flipbook', 'FlipBookController');

// });



// Route group for API versioning
Route::group(array('prefix' => 'api/dbp', 'before' => 'auth.basic'), function()
{
    Route::resource('book', 'BooksBookController@api');
});