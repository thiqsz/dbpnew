
  <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Books / Book</h1>
      <h2 class="">Display Books</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Books</a></li>
        <li class="active">Book</li>
      </ol>
    </div>
  </div>
  <div class="container clear_both padding_fix"> 
    
    <div id="main-content">
      <div class="page-content">

        <div class="row">
          <div class="col-md-12">@if (Session::has('message'))
<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
    </div>
           
            <div class="block-web">
              <div style="padding:0px;" class="header">
                <h3 style="background: #f2f2f2 url({{ URL::asset('images/grey.png') }});padding:30px 0px;text-align:center;font-weight:bold;" class="content-header">{{ strtoupper($books->title) }}</h3>
              </div>
              <div class="porlets-content">
                <div class="adv-table editable-table ">
                  <div class="clearfix">
                    <div class="btn-group pull-right">
                    {!! link_to(URL::previous(), 'Kembali', ['class' => 'btn btn-primary']) !!}
                    </div>
                  </div>
                  <div class="margin-top-10"></div>
                  <img width="35%" style="vertical-align:top;" src='{{ asset($books->pic) }}'>
                  <table style="display:inline-block;width:55%;margin-left:50px" class="table table-striped table-hover" id="editable-sample">
                    <tbody>
                      <tr>
                        <th class="col-sm-3 col-xs-3">Kategori</th>
                        <td class="col-sm-9 col-xs-9">{{$books->category_name}}</td>
                      </tr>
                      <tr>
                        <th>Penulis</th>
                        <td>{{$books->author_name}}</td>
                      </tr>
                      <tr>
                        <th>Penerbit</th>
                        <td>{{$books->publisher_name}}</td>
                      </tr>
                      <tr>
                        <th>Peringkat</th>
                        <td>{{$books->level_name}}</td>
                      </tr>
                      <tr>
                        <th>Bil Halaman</th>
                        <td>{{$books->pages}}</td>
                      </tr>
                      <tr>
                        <th>ISBN</th>
                        <td>{{$books->isbn}}</td>
                      </tr>
                      <tr>
                        <th>Keterangan Produk</th>
                        <td>{{strip_tags($books->summary)}}</td>
                      </tr>
                    </tbody>
                  </table>
                    {!! link_to(URL::previous(), 'Kembali', ['class' => 'btn btn-primary','style' => 'margin-top:10px;margin-left:93%']) !!}

                </div>
              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>