<?php
        use Illuminate\Support\Facades\Auth;
        ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php echo $header_script;?>
<body class="red_thm  fixed_header left_nav_fixed">
<div class="wrapper">
  <div class="header_bar">
    <div class="brand">
      <div class="logo" style="display:block"><span class="theme_color">DBP v1</span></div>
      <div class="small_logo" style="display:none"><img src="images/s-logo.png" width="50" height="47" alt="s-logo" /> <img src="{{ URL::asset('images/r-logo.png') }}" width="122" height="20" alt="r-logo" /></div>
    </div>
    <div class="header_top_bar"> <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i></a>
    
      <a href="{{ URL('books/book/create') }}" class="add_user"> <i class="fa fa-plus-square"></i> <span> Tambah Buku Baru</span></a>
      <div class="top_right_bar">
        <div class="user_admin dropdown"> <a href="javascript:void(0);" data-toggle="dropdown"><div class="avatar_img"><img src="{{ URL::asset('images/avatar/admin/shiroe.png') }}" /></div><div class="user_adminname">{{ Auth::user()->name }}</div> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <div class="top_pointer"></div>
            <!--<li> <a href="profile.html"><i class="fa fa-user"></i> Profile</a></li>
            <li> <a href="help.html"><i class="fa fa-question-circle"></i> Help</a></li>
            <li> <a href="settings.html"><i class="fa fa-cog"></i> Setting </a></li>-->
            <li> <a href="{{ URL('/logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
        </div>
    </div>
    </div>
  </div>
  <div class="inner">
    <div class="left_nav"><?php echo $side_menu;?></div>
    <div class="contentpanel"> 
    <?php echo $body;?> </div>
  </div>
</div>

<?php echo $footer_script;?>
</body>
</html>
