// Filename: RackView
//Likulli amalin syirrah, Walikulli syirratiin fatrah, Famankaanat fatrotuhu ila sunnati, Faqodihtada..
define([
  'jquery',
  'underscore',
  'backbone',
  'blazy',
  'text!module/rack/rack_template.html',
  'text!module/rack/temp/card.html',
  'text!module/master/others/noInternet_template.html',
  'text!module/master/others/notification_template.html',
  'module/detail/detailView',
  'module/Model/BookCollection'
], function($, _, Backbone,Blazy,template,cardTemplate,noInternetTemplate,notificationTemplate,DetailView,BookList){
    
  
    var RackView = Backbone.View.extend({
        el: null,
        
        initialize:function(el){
            console.log('laod Rack Content..');
            this.$el =el;
            _.bindAll(this, 'detectLoadMore');
             $(window).scroll(this.detectLoadMore);
        },
         events:{
             "click .card":"viewBookDetail"
        },
        
        render: function(category){
            this.category = category;
            this.bookNo=0;
            this.qBook=0;
            this.currentQBook=0;
            this.finishedScroll=true;        
            var data={};
            var compiledTemplate = _.template(template);
            this.$el.html(compiledTemplate());
            this.cardListRender(this.qBook);
            $(window).scrollTop(0);
        },
        
        viewBookDetail:function(e){
            if(typeof detailView!= 'undefined'){
                delete detailView;
            }
            var bookId = $(e.currentTarget).data('bookid');
            console.log(bookId);
           
            detailView = new DetailView(bookId,this.category);
            detailView.render();
        },
        
        loadInternetError:function(){
            var temp = _.template(noInternetTemplate);
            $('#cardList').html(temp());
            this.finishedScroll=false;
        },
        
        renderNotification:function(msg){
               var data={
                    message:msg
               };
               var compiledTemplate = _.template(notificationTemplate);
               $('#notification').html(compiledTemplate(data));
               $('.toast').addClass('toast-show');
               toastHide(6000,$(this));
        },
        
        
        
        loadImage:function(){
            
           blazy = new Blazy({
                container: '.bookthumb' ,// Default is window
                success: function(){
                    console.log('loading image');
                },
                error: function(ele, msg){
                    console.log(msg);
                }

            });
            
        },
        
        detectLoadMore:function(){           
                    console.log("load more.....");                  
                    console.log(this.qBook +"--" +this.currentQBook+'('+this.category+')');
                    that= this;
                    console.log(this.finishedScroll);
                    bookList = new BookList(this.category,this.qBook);
                    
                    var onDataHandler = function(bookList){
                       
                        var data={
                                    bookList:bookList.toJSON()
                                 };
                        
                        var temp = _.template(cardTemplate);
                        
                        console.log(data.bookList.length);
                        if(data.bookList.length>0){
                            $('#loadMoreCardList').replaceWith(temp({loadStatus:"loadMore",defaultBookList:data.bookList}));
                            that.loadImage();
                            that.qBook += data.bookList.length;
                            that.finishedScroll = true;
                            
                        }else{
                            console.log(data.bookList.length);
                            that.finishedScroll = false;
                        }
                        
//                        that.currentQBook++;
 
                    }
                    
                    var onErrorHandler = function(collection, response, options) {
                        console.log("error load more");
                        this.finishedScroll = false;
                    }
                 if(($(window).height()+ $(window).scrollTop())>0.75*$(document).height()&&this.finishedScroll){
                    bookList.fetch({ error:onErrorHandler, success : onDataHandler});
                      this.finishedScroll = false;
                    }
            
           
            
        },
        
        //!!DELETE
        test:function(){
            console.log('test');            
        },
        
        testDummyMaxValue:function(no,max){
          return(no<=max);
        },
        //-delete-!!
        
      
        
        cardListRender:function(qBook){
            that=this;
            bookList = new BookList(this.category,qBook); //url request cat=category&start=bookNo //resource.sampleBookListUrl+this.category+"_"+qBook+'.js'
       
            var onDataHandler = function(bookList){
                
                var data={
                            bookList:bookList.toJSON()
                         };
                
                var temp = _.template(cardTemplate);
                $('#cardList').html(temp({loadStatus:"init",defaultBookList:data.bookList}));
                that.loadImage();
                that.qBook +=data.bookList.length;
            }
            
            var onErrorHandler = function(collection, response, options) {
                console.log(response);
                if(response.status==404){
                  console.log("error page request"); 
                  that.renderNotification(resource.msg404Error);
                }else{
                  that.loadInternetError();   
                }
                //render warning
                
            }
            
            bookList.fetch({ error:onErrorHandler, success : onDataHandler,reset:true}); 
            
        }
 
  });
  // Our module now returns our view
  return RackView;
});