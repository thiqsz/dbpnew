<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model {
	protected $table = 'dbp_authors';
	protected $primaryKey = 'author_id';


	public function books()
    {
        return $this->belongsToMany('App\Author','dbp_books_dbp_authors','author_id','dbp_books_id');
    }

}
