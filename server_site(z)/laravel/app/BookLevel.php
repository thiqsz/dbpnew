<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BookLevel extends Model {

	protected $table = 'dbp_books_level';
	protected $primaryKey = 'level_id';

}
