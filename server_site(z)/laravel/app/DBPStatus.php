<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DBPStatus extends Model {

	protected $table = 'dbp_status';
	protected $primaryKey = 'status_id';

}
