<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DBPUsers extends Model {

    protected $table = 'dbp_users';
	protected $primaryKey = 'id';

	public function level()
    {
        return $this->belongsTo('App\UserLevel','level','code');
    }

    public function statusCode()
    {
        return $this->hasOne('App\UserStatus','shortcode','status');
    }


}
