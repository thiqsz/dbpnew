<?php namespace App\Http\Controllers;

use Request;
use App\Books;
use App\BooksImages;
use App\DBPUsers;
use Validator;
use File;
use Input;
use Auth;
use URL;
use Mail;
use Carbon;

class APIServiceController extends Controller {


	protected $hideFieldsCategory = [
							"isbn","cat_id",
							"addby","pub_id",
							"auth_id","level",
							"thumb","created_at",
							"updated_at","epub","pdf","status"
						];

	protected $selectFieldsCategory =["id","title","price","year","pages","pic","summary"];

	protected $selectFieldsBookDetails =["id","title","price","year","pages","pic","summary","epub","pdf"];

	protected $msgOK = array('response'=>'OK');
	protected $msgKO = array('response'=>'KO');

	public function category()
    {

    	// $test1 = array("a"=>"aa","b"=>"bb","c"=>"cc");
    	// $filterExcept = array("a");

    	// dd(array_diff_key($test1, array_flip($filterExcept)));
      
        $cat = Request::get('cat');
        $start=Request::get('start');
        $except=Request::get('except');
        // $bookcat = Category::get();
        // $bookarray = array('Category' => array($this->categorylist($bookcat) => $this->booklist($bookcat)));

        // allBooks
        //dd(Books::find(93)->authors->author_name);
        if($cat === 'all' || $cat === null){
            $allBooks = Books::with('authors','categories')
                        ->where('status', '=', 1)
                        ->select($this->selectFieldsCategory)
                        ->skip($start)
                        ->take(5)
                        ->orderBy('created_at','desc')
                        ->get();
        }else{

        	if(!$except){
        		$except=0;
        	}
        
            $this->cat = $cat;
            $cats = explode(',', $this->cat);
            $allBooks = array();
            foreach ($cats as $c) {
                $this->c =$c;
                $books = Books::with('authors','categories')
                	->select($this->selectFieldsCategory)
                    ->where('status', '=', 1)
                    ->where('id','!=',$except)
                    ->whereHas('categories',function($q)
                    {
                        $q->where('category_name', 'like', '%'.$this->c.'%');


                    })
                    ->skip($start)
                    ->take(5)
                    ->orderBy('created_at','desc')
                    ->get();
            
                foreach ($books as $book) {
                   array_push($allBooks, $book);
                }
                
            }
        
        }

        $allBooks =  $this->simplyBooksAuthorsAndCategories($allBooks);

        return response()->json($allBooks)
        ->setCallback(Request::input('callback'));

    }

    public function bookDetails($id=null)
    {
        $book = Books::with('authors','categories')
        		->select($this->selectFieldsBookDetails)
                ->where('status', '=', 1)
                ->where('id','=',$id)
                ->get();
        $book[0]['samples'] = BooksImages::where('books_id','=',$id)->get();
        $book=$this->simplyBooksAuthorsAndSamples($book[0]);
        return response()->json($book)
        ->setCallback(Request::input('callback'));

    }


     private function simplyBooksAuthorsAndCategories($books){
        foreach ($books as $book) {
           $authors = array();
           $categories = array();

            foreach($book->authors as $author){
                array_push($authors, $author->author_name);
                 
            }
            foreach($book->categories as $cat){
                array_push($categories, $cat->category_name);
            }
           unset($book['authors']);
           unset($book['categories']);
        
           $book['authors'] = $authors;
           $book['categories'] = $categories;
        }

        return $books;
    }

    private function simplyBooksAuthorsAndSamples($book){
        $samples = array();
        $authors = array();
        $categories = array();
       
        foreach ($book->samples as $sample) {
           array_push($samples, $sample->images_path);
        }

        foreach ($book->authors as $author) {
           array_push($authors, $author->author_name);
        }

        foreach($book->categories as $cat){
            array_push($categories, $cat->category_name);
        }

        unset($book['authors']);
        unset($book['samples']);
        unset($book['categories']);
        $book['authors'] = $authors;
        $book['samples'] = $samples;
        $book['categories'] = $categories;

        return $book;

    }



    //API controller
    public function login()
    {
    
    	$rules = array(
           
            'username'=>'required|email',// make sure the email is an actual email
            'password'=>'required'
        );

        $validator = Validator::make(Input::all(), $rules);


    	if(!$validator->passes()){
    		$msg = array("login"=>'KO','status'=>405,"msg"=>$validator);
    	}else{
    		$userdata = array(
	            'email'     => Input::get('username'),
	            'password'  => Input::get('password')
	        );


	        if (Auth::attempt($userdata)) {
	            $msg =array('login'=>'OK','status'=>200,'details'=>Auth::user());
	        } else {
	            $msg =array('login'=>'KO','status'=>401);        }


    	}
        
       return response()->json($msg)
        ->setCallback(Request::input('callback'));

    }

    public function logout(){
        Auth::logout();

        // !!!!!!!!response
    }

     public function register(){

         $rules = array(
            'name'   => 'required',
            'email'=>'required|email',// make sure the email is an actual email
            'password'=>'required|alpha_num|between:6,12|confirmed',
            'password_confirmation'=>'required|alpha_num|between:6,12'
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);
        $msg = array('register'=>'KO','msg'=>$validator->messages());
        if ($validator->passes()) {
            $user = new DBPUsers();
            $user->name = Input::get('name');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            try{
                $user->save();
                 $msg = array('register'=>'OK','msg'=>'successful');
            }catch(\Exception $error){
                $msg = array('register'=>'KO','msg'=>'fail');
            }
           
        }

        return response()->json($msg)
        ->setCallback(Request::input('callback'));
    }

    public function forgotPassword(){

    	$email = Input::get('email');

        $msgOK=$this->msgOK;
        $msgKO=$this->msgKO;
        $msgOK['message'] = 'Sila semak email untuk pembaharuan kata laluan.';
        $msgKO['message'] = 'E-mel ahli tidak didaftarkan lagi.';

        $msg=$msgKO;

        //find via email
        $rules= array(
        	'email'=>'required|email'
        );

       $validator =Validator::make(array("email"=>$email),$rules);
       if($validator->passes()){
       	$user =  DBPUsers::where('email','=',$email)->firstOrFail();

       	if($user){
       		//send link to email
       		$token =  md5(uniqid(mt_rand(), true));
       		$link = URL::to('/password/change/'.$token);
       		$time = Carbon\Carbon::now();
       	
       		$user->remember_token = $token;
       		$user->token_datetime = $time;

       		if($user->save()){
	       		$data['name'] = $user->name; // email content and token
	       		$data['link'] = $link;
	       		$data['email'] = $user->email;

	       		$this->data = $data;
				Mail::send('emails.forgot', $data, function($message)
				{
				    $message->to($this->data['email'], $this->data['name'])->subject('Pembaharuan kata laluan iDBP');
				});

				$msg = $msgOK;
       		}
       		
       	}
       	
       }
        

        return response()->json($msg)
        ->setCallback(Request::input('callback'));
    }




}