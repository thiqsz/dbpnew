<?php namespace App\Http\Controllers;

use Session;
use Validator;
use Input;
use Redirect;
use App\Books;
use App\Author;
use App\Category;
use App\BookLevel;
use App\Publishers;
use View;
use Response;
use File;
use Request;

class BooksCategoryController extends Controller {
		/**
		 * Display a listing of the resource.
		 *
		 * @return Response
		 */
		public function index()
		{
      $category = Category::all();
  		return View::make('index') 
      ->nest('header_script', 'header_script')
      ->nest('side_menu', 'side_menu')
      ->nest('body', 'books.category', array('category' => $category))
      ->nest('footer_script', 'footer_script_datatables');
		}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return Response
		 */
		public function create()
		{
			  return View::make('index')  
	      ->nest('header_script', 'header_script')
	      ->nest('side_menu', 'side_menu')
	      ->nest('body', 'books.category.create')
	      ->nest('footer_script', 'footer_script_forms');
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @return Response
		 */
		public function store()
		{
        if (Request::ajax()) {
              $rules = array(
	             'category_name' => 'required'
	        );
	        $validator = Validator::make(Input::all(), $rules);
	        if ($validator->fails()) {
	            return Redirect::to('books/category/create')
	                ->withErrors($validator);
	        } else {
	            $category = new Category;
	            $category->category_name = Input::get('category_name');
	            $category->save();
                $result = [
                'catname' => Input::get('category_name'),
                'catid' => $category->cat_id
                ];
                   return Response::json($result);
	       }
    

         }else{
	        $rules = array(
	            'category_name' => 'required'
	        );
	        $validator = Validator::make(Input::all(), $rules);
	        if ($validator->fails()) {
	            return Redirect::to('books/category/create')
	                ->withErrors($validator);
	        } else {
	            $category = new Category;
	            $category->category_name = Input::get('category_name');
	            $category->save();

	            // redirect
	            Session::flash('message', 'Successfully add new category!');
	            return Redirect::to('books/category');
	            }
		}
        }
		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function show($id)
		{
			//
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function edit($id)
		{
	      //get the current book by id
        // $category = Category::where('cat_id', $cat_id)->get();
			$category = Category::find($id);
        if (is_null($category))
        {
            return Redirect::to('books/category');
        }
        // redirect to update form
        return View::make('index')
        ->nest('header_script', 'header_script')
        ->nest('side_menu', 'side_menu')
        ->nest('body', 'books.category.edit', array('category' => $category))
        ->nest('footer_script', 'footer_script_datatables');
        
		}
		

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function update($id)
		{
			// validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'category_name'=> 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('books/category' . $id . '/edit')
                ->withErrors($validator);
        } else {
            // store
            $category = Category::find($id);
            $category->category_name= Input::get('category_name');
            $category->save();

            // redirect
            Session::flash('message', 'Successfully updated nerd!');
            return Redirect::to('books/category');
        }
    }
		

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function destroy($id)
		{
			//delete category
			        Category::find($id)->delete();
			        return Redirect::to('books/category')
			            ->withInput()
			            ->with('message', 'Successfully deleted Book.');
		}
	    
	    

	}