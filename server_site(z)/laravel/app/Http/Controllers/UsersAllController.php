<?php namespace App\Http\Controllers;

use Session;
use Validator;
use Input;
use Redirect;
use App\DBPUsers;
use View;
use Response;
use Request;
use Auth;
use Storage;
use Hash;
use Mail;
use Config;
use App;
use URL;

class UsersAllController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

			$allusers = DBPUsers::with('level','statusCode')
			->whereHas('level', function($q)
				{
				    $q->where('code', '=', 'member');

				})

				->get();

				// dd($allusers);
		    return View::make('index')  
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'users_all',array('allusers'=>$allusers))
            ->nest('footer_script', 'footer_script_datatables');
	}

	public function adminIndex()
	{
			$allusers = DBPUsers::with('level')
				->where('level','=','admin')
				->get();
			
		    return View::make('index')  
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'users_all',array('allusers'=>$allusers))
            ->nest('footer_script', 'footer_script_datatables');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
			$register= array('name'=>'','email'=>'');

		    return View::make('index')  
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'users.create',array('register'=>$register))
            ->nest('footer_script', 'footer_script');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = array(
            'name' => 'required',
            'email' => 'required|email|unique:dbp_users',
            'password'=>'required|alpha_num|between:6,12|confirmed',
            'password_confirmation'=>'required|alpha_num|between:6,12'
       
        );

		$validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('users/member/create')
            	->withInput()
                ->withErrors($validator);
        } 

        $user =  new DBPUsers();
        $user->name = Input::get('name');
        $user->email = Input::get('email');
        $user->username = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->level= 'member';
        $user->status = 'IA';
        $user->remember_token = preg_replace('/[^a-zA-Z0-9]+/', '', Hash::make(Input::get('email')));
        
        if($user->save()){
        	$data = array('name'=>$user->name,"link"=>URL::to('/').'/verify/'.$user->remember_token,
        		"email"=>$user->email);
        	$this->sendRegisterEmail($data);

        	return Redirect::to('users/members')->with('message', 'Ahli baru berjaya didaftarkan. Pastikan ahli menyemak e-mel untuk melakukan pengesahan.');;
        }

		
	}

	public function verifyEmail($token){

		$user = DBPUsers::where('remember_token','=',$token)
				->where('status','=','IA')
				->get();

		if(!empty($user[0])){
			$user = $user[0];

			$user->status = 'A';
			$user->remember_token = null;
			$user->save();

			//redirect to TQ page
			return View::make('users.verified');
		}

		return Response::make("Not Found", 404);
	}

	private function sendRegisterEmail($data){

		$this->data = $data;
		Mail::send('emails.register', $data, function($message)
		{
		    $message->to($this->data['email'], $this->data['name'])->subject('Pengesahan Pendaftaran iDBP');
		});
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$member = DBPUsers::where('id',"=",$id)
					->where('level','=','member')->first();

		$data = array('name'=>'','email'=>'','status'=>null);

		if($member){
			$data['name']=$member->name;
			$data['email']=$member->email;
			$data['status']=$member->status;
			$data['id']=$member->id;
		}

		return View::make('index')
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'users.edit', $data)
            ->nest('footer_script', 'footer_script_forms');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$member = DBPUsers::where('id',"=",$id)
					->where('level','=','member')->first();
		$isNewPassword =false;
		if($member){
			$rules = array(
				'email' => 'required|email',
				'name'	=>  'required'
				);

			if($member->email != Input::get('email')){
				$rules = array(
				'email' => 'required|email|unique:users',
				'name'	=>  'required'
				);
			}

			if(!empty(Input::get('password'))){
        		$rulesPassword = array(
        			'password'=>'required|alpha_num|between:6,12|confirmed',
        			'password_confirmation'=>'required|alpha_num|between:6,12'
        		);

        		$rules=array_merge($rules,$rulesPassword);
        		$isNewPassword = true;
	        }

	       
			
			 $validator = Validator::make(Input::all(), $rules);
	        if ($validator->fails()) {
	            return Redirect::to('users/member/'.$id.'/edit')
	            	->withInput()
	                ->withErrors($validator);
	        } else {
	        	
				$member->name = Input::get('name');
				$member->email = Input::get('email');
				$member->status = Input::get('status');
				$member->username = Input::get('email');
				//check new password
				if($isNewPassword){
					$member->password = Hash::make(Input::get('password'));
				}

				if($member->save()){
					return Redirect::to('users/members');
				}
	        }
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$member = DBPUsers::find($id);
		$member->delete();

		return Redirect::to('users/members')
            ->withInput()
            ->with('message', 'Ahli dipilih berjaya dipadam.');
	}


	public function viewChangePassword($user_token){

		$user = DBPUsers::where('remember_token','=',$user_token)
			->where('status','=','A')
			->firstOrFail();

		return View::make('users.resetpassword',array(
				'token' => $user_token
			));
    }
    
    //post changed password 
    public function doChangePassword(){
    	
    	////select user and ensure its already activated user by token
    	$rulesPassword = array(
        			'password'=>'required|alpha_num|between:6,12|confirmed',
        			'password_confirmation'=>'required|alpha_num|between:6,12',
        			'user_token' => 'required'
        		);

    	 $validator = Validator::make(Input::all(), $rulesPassword);
    	 $user_token = Input::get('user_token');



        if ($validator->fails()) {
        	return Redirect::to('/password/change/'.$user_token.'/proceed')
                ->withErrors($validator);
        }
	
		$user = DBPUsers::where('remember_token','=',$user_token)
			->where('status','=','A')
			->firstOrFail();


    	 // check token duration
		$time = strtotime($user->token_datetime);
		$curtime = time();
		if($curtime-$time > 86400){
			return View::make('users.resetfail',array('message'=>'Tempoh pembaharuan kata laluan telah tamat. Untuk penukaran kata laluan, sila jana pautan pada aplikasi iDBP'));
    
		} 

		if($user){
			
			$user->password =  Hash::make(Input::get('password'));
			$user->remember_token =null;
			$user->token_datetime = null;

			if($user->save()){
				//finish session by user
				Auth::logout($user);
				// save changed
				return View::make('users.resetsuccess');
			}

		}

    	// return fail
    	return View::make('users.resetfail',array('message'=>'Kata laluan anda TIDAK berjaya ditukar'));
    }
    

}
