<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model {

    protected $table = 'dbp_user_status';
	protected $primaryKey = 'sid';

}
