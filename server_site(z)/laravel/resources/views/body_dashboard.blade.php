      <div class="pull-left breadcrumb_admin clear_both">
        <div class="pull-left page_title theme_color">
          <h1>Dashboard</h1>
        </div>
        <div class="pull-right">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">DASHBOARD</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </div>
      </div>
      <div class="container clear_both padding_fix">
        <div class="row">
          <div class="col-sm-3 col-sm-6">
            <div class="information green_info">   
              <div class="information_inner">
              	<div class="info green_symbols"><i class="fa fa-book icon"></i></div>
                <span>JUMLAH BUKU </span>
                <h1 class="bolded">{{ $books }}</h1>
                <div class="infoprogress_green">
                  <div class="greenprogress"></div>
                </div>
                <div class="pull-right" id="work-progress1">
                  <canvas style="display: inline-block; width: 47px; height: 25px; vertical-align: top;" width="47" height="25"></canvas>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-sm-6">
            <div class="information blue_info">
              <div class="information_inner">
              <div class="info blue_symbols"><i class="fa fa-book icon"></i></div>
                <span>JUMLAH BUKU BARU </span>
                <h1 class="bolded">{{ $booknew2week }}</h1>
                <div class="infoprogress_blue">
                  <div class="blueprogress"></div>
                </div>
                <div class="pull-right" id="work-progress2">
                  <canvas style="display: inline-block; width: 47px; height: 25px; vertical-align: top;" width="47" height="25"></canvas>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-sm-6">
            <div class="information red_info">
              <div class="information_inner">
              <div class="info red_symbols"><i class="fa fa-building icon"></i></div>
                <span>JUMLAH PENERBIT </span>
                <h1 class="bolded">{{ $publisher }}</h1>
                <div class="infoprogress_red">
                  <div class="redprogress"></div>
                </div>
                <div class="pull-right" id="work-progress3">
                  <canvas style="display: inline-block; width: 47px; height: 25px; vertical-align: top;" width="47" height="25"></canvas>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-sm-6">
           <div class="information gray_info">
              <div class="information_inner">
              <div class="info gray_symbols"><i class="fa fa-users icon"></i></div>
                <span>JUMLAH PENGGUNA </span>
                <h1 class="bolded">{{ $usercount }}</h1>
                <div class="infoprogress_gray">
                  <div class="grayprogress"></div>
                <div class="pull-right" id="work-progress4">
                  <canvas style="display: inline-block; width: 47px; height: 25px; vertical-align: top;" width="47" height="25"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>       
     
        
 
        
      </div>
<div class="row">
          <div class="col-md-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h4>Judul-Judul Terbaru</h4>
              </div>
              <div class="panel-body" id="cardcolumnss">
               		@foreach($booknew as $bookarrival)
                		<div class="pin">
                			<a href=" {{ url('books/book',$bookarrival->id)}}"><img src="{{ URL::asset($bookarrival->pic) }}" /></a>
                			<div>
                				{{ $bookarrival->title }}
                			</div>
                		</div>
             @endforeach
              </div>
            </div>
          </div>
  <!-- <div class="col-md-4 ">
            <div class="block-web green-bg-color">
              <h3 class="content-header ">Pintasan</h3>
              <div class="porlets-content" id="cardcolumns">
             <div class="pinshortcut">
			<i class="fa fa-book icon"></i> 
			<p>
				Buku-buku
			</p>
		    </div>
              </div>
            </div>
           
          </div>-->
        </div>   