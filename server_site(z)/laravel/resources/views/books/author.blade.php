  <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Buku / Penulis</h1>
      <h2 class="">Senarai Penulis</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Buku</a></li>
        <li class="active">Penulis</li>
      </ol>
    </div>
  </div>
  <div class="container clear_both padding_fix"> 
    
    <div id="main-content">
      <div class="page-content">
      @if ( Session::has('flash_message') )
        <div class="alert {{ Session::get('flash_type') }}">
            <h3>{{ Session::get('flash_message') }}</h3>
        </div>
        
      @endif
                <!-- @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif -->
        <div class="row">
         
           
            <div class="block-web col-md-8">
              <div class="header">

                <h3 class="content-header">Senarai Penulis</h3>
              </div>
              <div class="porlets-content ">
                <div class="adv-table editable-table ">
                  <div class="clearfix">
                    <div class="btn-group">
                      <button onclick="newAuthor()" class="btn btn-primary"> Tambah <i class="fa fa-plus"></i> </button>
                    </div>
                    
                  </div>
                  <div class="margin-top-10"></div>
                  <table class="table table-striped table-hover table-bordered " id="editable-sample">
                    <thead>
                      <tr>
                        <th>Author Name</th>
                        <th class="center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($author as $value)
                    <tr>
                      <td>{{ $value->author_name }}</td>
                      <td class="center">
                        {!! link_to_route('books.author.edit', 'Kemaskini', array($value->author_id),array('class' => 'btn btn-xs btn-info')) !!}
                        {!! Form::open(array('style' => 'display:inline-block','method'=> 'DELETE', 'url' =>array('books/author', $value->author_id))) !!}
                        {!! Form::submit('Padam', array('class' => 'btn btn-xs btn-danger')) !!}
                        {!! Form::close() !!}
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                    
                  </table>
                </div>
              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>
<script>
function newAuthor(){
    window.location.href="{{ URL('books/author/create') }}";
}
</script>