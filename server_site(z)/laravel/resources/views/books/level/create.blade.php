<?php 
use Illuminate\html;
?>
      <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Buku / Peringkat / Tambah</h1>
      <h2 class="">Tambah Peringkat Baru</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Buku</a></li>
        <li><a href="{{ URL('books/level') }}">Peringkat</a></li>
        <li class="active">Tambah</li>
      </ol>
    </div>

  <div class="container clear_both padding_fix"> 


    <div id="main-content">
      <div class="page-content">
         </div>@if($errors->any())
<div class="alert alert-danger">{{$errors->first()}}</div>
@endif
        <div class="row">
          <div class="col-md-12">
<div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a><a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Tambah Peringkat Baru</h3>
            </div>
            <div class="porlets-content">
             {!! Form::open(array('url' => 'books/level', 'class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data')) !!}
            
                <div class="form-group">
                    {!! Form::label('level_name', 'Peringkat', array('class' => 'col-sm-2 control-label')) !!}
                     
                  <div class="col-sm-5">
                      {!! Form::text('level_name', '', array('class' => 'form-control', 'id' => 'level_name')) !!}
                  </div>
                </div>
             <div class="bottom">
                 {!! Form::label('', '', array('class' => 'col-sm-2 control-label')) !!}
                  <button type="submit" class="btn btn-primary">Hantar</button>
                  <button type="button" class="btn btn-default" onclick="backbtn()">Batal</button>
                </div><!--/form-group-->
              {!! Form::close() !!}
            </div>
          </div>

          </div>

        </div>

      </div>

    </div>

  </div>
<script>
function backbtn(){
    window.location.href="{{ URL('books/level') }}";   
}
</script>