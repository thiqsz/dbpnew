  <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Books / Publisher</h1>
      <h2 class="">Display All Publisher</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Books</a></li>
        <li class="active">Publisher</li>
      </ol>
    </div>
  </div>
  <div class="container clear_both padding_fix"> 
    
    <div id="main-content">
    @if ( Session::has('flash_message') )
      <div style="padding:5px;margin-bottom:0px;" class="alert {{ Session::get('flash_type') }}">
          <h3 style="font-size:20px;">{{ Session::get('flash_message') }}</h3>
      </div>
      
    @endif
      <div class="page-content">
      
                <!-- @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif -->
        <div class="row">
          <div class="col-md-12">
          
    </div>
           
            <div class="block-web">
              <div class="header">
                <h3 class="content-header">Senarai Penerbit</h3>
              </div>
              <div class="porlets-content">
                <div class="adv-table editable-table ">
                  <div class="clearfix">
                    <div class="btn-group">
                      <button onclick="newPublisher()" class="btn btn-primary"> Tambah <i class="fa fa-plus"></i> </button>
                    </div>
                    <div class="btn-group pull-right">
                      
                    </div>
                  </div>
                  <div class="margin-top-10"></div>
                  <table class="table table-striped table-hover table-bordered" id="editable-sample">
                    <thead>
                      <tr>
                        <th>Publisher Name</th>
                        <th class="center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($publisher as $value)
                    <tr>
                      <td>{{ $value->publisher_name }}</td>
                      <td class="center">
                        {!! link_to_route('books.publisher.edit', 'Update', array($value->pub_id),array('class' => 'btn btn-xs btn-info')) !!}
                        {!! Form::open(array('style' => 'display:inline-block','method'=> 'DELETE', 'url' =>array('books/publisher', $value->pub_id))) !!}
                        {!! Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) !!}
                        {!! Form::close() !!}
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                    
                  </table>
                </div>
              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>
<script>
function newPublisher(){
    window.location.href="{{ URL('books/publisher/create') }}";
}
</script>