
  <div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
      <h1>Pengguna </h1>
      <h2 class="">Ahli</h2>
    </div>
    <div class="pull-right">
      <ol class="breadcrumb">
        <li><a href="#">Pengguna</a></li>
        <li class="active">Ahli</li>
      </ol>
    </div>
  </div>
  <div class="container clear_both padding_fix"> 
    <!--\\\\\\\ container  start \\\\\\-->
       
    <div id="main-content">

      <div class="page-content">  
        <div class="row">
        
          <div class="col-md-12">
            <div class="block-web">
 @if (Session::has('message'))
<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
              <div class="header">
                <h3 class="content-header">Senarai Ahli</h3>
              </div>
              <div class="porlets-content">
                <div class="adv-table editable-table ">
                  <div class="clearfix">
                    <div class="btn-group">
                      <button onclick="newMember()" class="btn btn-primary"> Tambah <i class="fa fa-plus"></i> </button>
                    </div>
                    <div class="btn-group pull-right">
                     
                    </div>
                  </div>
                  <div class="margin-top-10"></div>
                  <table class="table table-striped table-hover table-bordered" id="editable-sample">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>E-Mel</th>
                        <th class="center">Status</th>
                        <th class="center" class="center" class="center">
                        Registration Date
                        </th>
                        <th class="center" class="center" class="center" class="center">
                        Action
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($allusers as $value)
                    <tr>
                      <td>{{ $value->name }}</td>
                      <td>{{ $value->email }}</td>
                      <td class="center">{{ $value->statusCode['description']}}</td>
      
                      <td class="center">{{ date('d M Y', strtotime($value->created_at)) }}</td>
                      <td class="center">
                        {!! link_to_route('users.member.edit', 'Kemaskini', array($value->id),array('class' => 'btn btn-xs btn-info','style'=>'margin-right:5px')) !!}
              
                        {!! Form::open(array('style' => 'display:inline-block','method'=> 'DELETE', 'url' =>array('/users/member', $value))) !!}
                        {!! Form::submit('Padam', array('class' => 'btn btn-xs btn-danger')) !!}
                        {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    
                  </table>
                </div>
              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>
  <script type="text/javascript">
function newMember(){
    window.location.href="{{ URL('users/member/create') }}";
}
  </script>
